import { useState, useEffect, useRef, useCallback } from 'react';

const useStickyHeader = (defaultSticky = false, elemHeigth = 88) => {
  const [isSticky, setIsSticky] = useState(defaultSticky);
  const elemRef = useRef(null);

  const toggleSticky = useCallback(
    ({ top, bottom }) => {
      if (top <= elemHeigth) {
        setIsSticky(true);
      } else {
        setIsSticky(false);
      }
    },
    [setIsSticky, elemHeigth],
  );

  const handleScroll = useCallback(() => {
    toggleSticky(elemRef.current.getBoundingClientRect());
  }, [toggleSticky]);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [toggleSticky, handleScroll]);

  return { elemRef, isSticky };
};

export default useStickyHeader;
