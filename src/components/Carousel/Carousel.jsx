// import { useRef } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import {
  Navigation,
  Scrollbar,
  FreeMode,
  Mousewheel,
  Autoplay,
  EffectFade,
} from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

// import { ArowRightIcon } from 'components/Icons';

import s from './Carousel.module.css';

import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/effect-fade';

function Carousel({
  className,
  children,
  spaceBetween,
  slidesPerView,
  navigation,
  arrowType,
  ...props
}) {
  // const navigationPrevRef = useRef(null);
  // const navigationNextRef = useRef(null);
  return (
    <div className={s.wrapper}>
      <Swiper
        modules={[
          Navigation,
          FreeMode,
          Scrollbar,
          Mousewheel,
          Autoplay,
          EffectFade,
        ]}
        spaceBetween={spaceBetween}
        slidesPerView={slidesPerView}
        navigation={navigation}
        // navigation={{
        //   prevEl: navigationPrevRef.current,
        //   nextEl: navigationNextRef.current,
        // }}
        // onBeforeInit={(swiper) => {
        //   // eslint-disable-next-line no-param-reassign
        //   swiper.params.navigation.prevEl = navigationPrevRef.current;
        //   // eslint-disable-next-line no-param-reassign
        //   swiper.params.navigation.nextEl = navigationNextRef.current;
        // }}
        className={cn(
          s.root,
          className,
          arrowType === 'square' ? 'square-nav' : 'rounded-nav',
        )}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
        // pagination={{ clickable: true }}
        // onSlideChange={() => console.log('slide change')}
        // onSwiper={(swiper) => console.log(swiper)}
      >
        {children.map((item, index) => (
          <SwiperSlide key={item.id || index}>{item}</SwiperSlide>
        ))}
      </Swiper>
      {/* {navigation ? (
        <>
          <div
            ref={navigationPrevRef}
            className={cn(
              s.button,
              s.prevButton,
              arrowType === 'square' ? s.square : s.rounded,
            )}>
            <ArowRightIcon />
          </div>
          <div
            ref={navigationNextRef}
            className={cn(
              s.button,
              s.nextButton,
              arrowType === 'square' ? s.square : s.rounded,
            )}>
            <ArowRightIcon />
          </div>
        </>
      ) : null} */}
    </div>
  );
}

Carousel.propTypes = {
  className: PropTypes.string,
  spaceBetween: PropTypes.number,
  slidesPerView: PropTypes.number,
  navigation: PropTypes.bool,
  arrowType: PropTypes.oneOf(['square', 'rounded']),
  children: PropTypes.arrayOf(PropTypes.node),
};

Carousel.defaultProps = {
  className: null,
  children: [],
  spaceBetween: 24,
  slidesPerView: 3,
  navigation: false,
  arrowType: 'rounded',
};

Carousel.displayName = 'Carousels';

export default Carousel;
