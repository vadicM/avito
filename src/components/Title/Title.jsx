import PropTypes from 'prop-types';

import cn from 'classnames';

import s from './Title.module.css';

function Title({ children, className, level = 1, weight }) {
  const classes = cn(
    s.root,
    s[`level-${level}`],
    s[`weight-${weight}`],
    className,
  );

  const Tag = `h${level}`;

  return <Tag className={classes}>{children}</Tag>;
}

Title.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  level: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
  weight: PropTypes.oneOf(['normal', 'medium', 'bold']),
};

Title.defaultProps = {
  children: null,
  className: null,
  level: 1,
  weight: 'normal',
};

Title.displayName = 'Title';

export default Title;
