/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-props-no-spreading */
import { useMemo } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { useSelect } from 'downshift';
import { DropdownArrowIcon } from 'components/Icons';
import Text from 'components/Text';
import s from './Select.module.css';

const itemToString = (item) => item?.value.title ?? '';

function Select({ options = [], label = '', value, onChange }) {
  const controlledSelectedItem = useMemo(
    () => options.find((option) => option.value === value),
    [options, value],
  );
  const {
    isOpen,
    getLabelProps,
    getMenuProps,
    getToggleButtonProps,
    highlightedIndex,
    getItemProps,
  } = useSelect({
    items: options,
    // https://github.com/downshift-js/downshift/blob/bcc793a194a7c30f84b4514b8541a6b7ee3d991f/src/utils.js#L442
    selectedItem: controlledSelectedItem || null, // null (not undefined) because controlled selectedItem must be value | null
    onSelectedItemChange: onChange,
    itemToString,
  });

  const valueClasses = cn(s.value, {
    [s.active]: isOpen,
  });

  const listClasses = cn(s.list, { [s.active]: isOpen });

  return (
    <div className={s.root}>
      <button
        className={valueClasses}
        type="button"
        {...getToggleButtonProps()}>
        {label ? (
          <Text {...getLabelProps()} className={s.label}>
            {label}
          </Text>
        ) : null}
        <div className={s.wrap}>
          <p className={s.text}>
            {controlledSelectedItem
              ? controlledSelectedItem.title
              : value.title}
          </p>
          <span className={cn(s.icon, isOpen && s.active)}>
            <DropdownArrowIcon />
          </span>
        </div>
      </button>

      <ul className={listClasses} {...getMenuProps()}>
        {isOpen
          ? options.map((item, index) => (
              <li
                key={item.title}
                className={cn(s.option, highlightedIndex === index && s.active)}
                {...getItemProps({ item, index })}>
                {item.title}
              </li>
            ))
          : null}
      </ul>
    </div>
  );
}

Select.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.shape({
    value: PropTypes.string,
    title: PropTypes.string,
  }),
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      title: PropTypes.string,
    }),
  ),
};

Select.defaultProps = {
  className: null,
  value: {},
  label: null,
  onChange: (elem) => null,
  options: [],
};

Select.displayName = 'Select';

export default Select;
