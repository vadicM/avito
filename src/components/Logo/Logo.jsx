import PropTypes from 'prop-types';
import cn from 'classnames';
import { Link } from 'react-router-dom';

import logo from 'assets/images/logo.svg';

import s from './Logo.module.css';

function Logo({ className, size }) {
  const imageWidth = size === 'medium' ? '106' : '159';
  return (
    <Link to="/" className={cn(s.root, className)}>
      <img src={logo} alt="avito" className={s.img} width={imageWidth} />
    </Link>
  );
}

Logo.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(['medium', 'large']),
};

Logo.defaultProps = {
  className: null,
  size: 'medium',
};

Logo.displayName = 'Logo';

export default Logo;
