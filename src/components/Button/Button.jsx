import PropTypes from 'prop-types';

import cn from 'classnames';

import s from './Button.module.css';

function Button({
  children,
  className,
  disabled,
  onClick,
  type,
  size,
  fullWidth,
  htmlType = 'button',
}) {
  const handleClick = (event) => {
    if (onClick) {
      onClick(event);
    }
  };

  const classes = cn(
    s.root,
    {
      [`${s[type]}`]: type,
      [`${s[size]}`]: size,
    },
    fullWidth && s.fullWidth,
    className,
  );

  return (
    <button
      // eslint-disable-next-line react/button-has-type
      type={htmlType}
      className={classes}
      disabled={disabled}
      onClick={handleClick}>
      {children || null}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  className: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(['primary', 'ghost', 'bordered']),
  htmlType: PropTypes.oneOf(['button', 'submit', 'reset']),
  size: PropTypes.oneOf(['small', 'medium', 'large', 'extra-large']),
};

Button.defaultProps = {
  children: null,
  disabled: false,
  className: null,
  onClick: null,
  type: 'primary',
  htmlType: 'button',
  size: 'medium',
  fullWidth: false,
};

Button.displayName = 'Button';

export default Button;
