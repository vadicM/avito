import PropTypes from 'prop-types';

import cn from 'classnames';

import s from './Text.module.css';

function Text({
  children,
  className,
  disabled,
  size,
  weight,
  inline = false,
  muted,
}) {
  const classes = cn(
    s.root,
    s[`weight-${weight}`],
    s[`size-${size}`],
    muted && s.muted,
    className,
  );

  const Tag = inline ? 'span' : 'p';

  return <Tag className={classes}>{children}</Tag>;
}

Text.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  inline: PropTypes.bool,
  muted: PropTypes.bool,
  className: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  weight: PropTypes.oneOf(['normal', 'medium', 'bold']),
};

Text.defaultProps = {
  children: null,
  disabled: false,
  inline: false,
  muted: false,
  className: null,
  size: 'medium',
  weight: 'normal',
};

Text.displayName = 'Text';

export default Text;
