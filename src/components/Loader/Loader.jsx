import PropTypes from 'prop-types';

import cn from 'classnames';

import { ReactComponent as LoaderIcon } from 'assets/icons/loader.svg';

import s from './Loader.module.css';

function Loader({ className, fullWidth, isSmall }) {
  return (
    <div
      className={cn(
        s.root,
        fullWidth && s.fullWidth,
        isSmall && s.small,
        className,
      )}>
      <LoaderIcon />
    </div>
  );
}

Loader.propTypes = {
  className: PropTypes.string,
  fullWidth: PropTypes.bool,
  isSmall: PropTypes.bool,
};

Loader.defaultProps = {
  className: null,
  fullWidth: false,
  isSmall: false,
};

Loader.displayName = 'Loader';

export default Loader;
