import { ReactComponent as VkIcon } from 'assets/icons/vk.svg';
import { ReactComponent as OkIcon } from 'assets/icons/ok.svg';
import { ReactComponent as TwiiterIcon } from 'assets/icons/twitter.svg';
import { ReactComponent as YoutubeIcon } from 'assets/icons/youtube.svg';
import { ReactComponent as TelegramIcon } from 'assets/icons/telegram.svg';
import { ReactComponent as HamburgerIcon } from 'assets/icons/humburger.svg';
import { ReactComponent as CloseIcon } from 'assets/icons/close.svg';
import { ReactComponent as ArrowRightIcon } from 'assets/icons/arrow-right.svg';
import { ReactComponent as ArrowLeftIcon } from 'assets/icons/arrow-left.svg';
import { ReactComponent as DropdownArrowIcon } from 'assets/icons/arrow-down.svg';
import { ReactComponent as PlayIcon } from 'assets/icons/play.svg';
import { ReactComponent as ArrowBackIcon } from 'assets/icons/arrow-back.svg';

export {
  VkIcon,
  OkIcon,
  TwiiterIcon,
  YoutubeIcon,
  TelegramIcon,
  HamburgerIcon,
  CloseIcon,
  ArrowRightIcon,
  ArrowLeftIcon,
  DropdownArrowIcon,
  PlayIcon,
  ArrowBackIcon,
};
