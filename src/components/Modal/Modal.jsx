/* eslint-disable react/jsx-props-no-spreading */
import Popup from 'reactjs-popup';
import PropTypes from 'prop-types';

import Button from 'components/Button';

import { CloseIcon } from 'components/Icons';

import s from './Modal.module.css';

function Modal({ children, lockScroll = true, open = false, ...props }) {
  return (
    <Popup
      open={open}
      modal
      lockScroll={lockScroll}
      className={s.root}
      position="top center"
      {...props}>
      {(close: () => void) => (
        <>
          <Button
            theme="ghost"
            onClick={close}
            className={s.closeButton}
            type="ghost">
            <CloseIcon />
          </Button>
          {children}
        </>
      )}
    </Popup>
  );
}

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  lockScroll: PropTypes.bool,
  open: PropTypes.bool,
};

Modal.defaultProps = {
  lockScroll: true,
  open: false,
};

Modal.displayName = 'Modal';

export default Modal;
