import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import s from './TextArea.module.css';

function TextArea({
  disabled = false,
  name,
  className,
  onChange,
  value,
  ...props
}) {
  const textareaRef = useRef(null);

  useEffect(() => {
    textareaRef.current.style.height = '0px';
    const { scrollHeight } = textareaRef.current;
    textareaRef.current.style.height = `${scrollHeight}px`;
  }, [value]);

  const classNames = cn(s.root, className, disabled && s.disabled);

  return (
    <textarea
      ref={textareaRef}
      className={classNames}
      name={name}
      id={name}
      value={value}
      onChange={onChange}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
    />
  );
}

TextArea.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  className: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

TextArea.defaultProps = {
  children: null,
  disabled: false,
  className: null,
  onChange: null,
  value: '',
};

TextArea.displayName = 'Text';

export default TextArea;
