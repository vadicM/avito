/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { useState } from 'react';
import PropTypes from 'prop-types';

import AnimateHeight from 'react-animate-height';

import cn from 'classnames';

import Button from 'components/Button';

import Title from 'components/Title';

import { DropdownArrowIcon } from 'components/Icons';
import s from './TabElement.module.css';

function TabElement({ className, title, children, bordered, shadow, full }) {
  const [height, setHeight] = useState(0);

  const handleToogleActive = () => {
    setHeight((prevState) => (prevState === 0 ? 'auto' : 0));
  };

  return (
    <div
      className={cn(
        s.root,
        bordered && s.bordered,
        shadow && s.shadow,
        full && s.full,
        height === 0 && s.hover,
        height === 'auto' && s.active,
        className,
      )}
      onClick={full ? handleToogleActive : () => null}>
      <Button
        type="ghost"
        onClick={!full ? handleToogleActive : () => null}
        className={s.button}>
        <Title level={5} className={s.title} weight="bold">
          {title}
        </Title>

        <span className={cn(s.icon, height === 'auto' && s.active)}>
          <DropdownArrowIcon />
        </span>
      </Button>
      <AnimateHeight duration={350} height={height}>
        <div className={s.content}>{children}</div>
      </AnimateHeight>
    </div>
  );
}

TabElement.propTypes = {
  className: PropTypes.string,
  bordered: PropTypes.bool,
  shadow: PropTypes.bool,
  full: PropTypes.bool,
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
};

TabElement.defaultProps = {
  className: null,
  bordered: false,
  shadow: false,
  full: false,
};

TabElement.displayName = 'TabElement';

export default TabElement;
