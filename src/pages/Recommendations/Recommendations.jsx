/* eslint-disable react/jsx-props-no-spreading */
import { useEffect, useContext } from 'react';

import filter from 'lodash.filter';

import Title from 'components/Title';
import Text from 'components/Text';
import Loader from 'components/Loader';
import CommonLayout from 'layouts/CommonLayout';

import GoBack from 'modules/sections/components/GoBack';
import SpecializationCard from 'modules/home/components/SpecializationCard';
import EventCard from 'modules/home/components/EventCard';

import SelectBlock from 'modules/recomendations/components/SelectBlock';

import useRecomendation from 'modules/recomendations/hooks/useRecomendation';
import useSpecialization from 'modules/sections/hooks/useSpecialization';

import { FilterContext } from 'context/FiltersContext';

import s from './Recommendations.module.css';

function Recommendations() {
  const { courses, onGetCourses, loadingCourses } = useRecomendation();
  const { specializations, onGetSpecializations, loadingSpecializations } =
    useSpecialization();

  useEffect(() => {
    onGetCourses();
  }, [onGetCourses]);

  useEffect(() => {
    onGetSpecializations();
  }, [onGetSpecializations]);

  const { valueExperience, valueBusiness, valueLearn } =
    useContext(FilterContext);

  const filteredSpecializations = filter(specializations, {
    fields: {
      ...(valueBusiness.value !== 'any' && {
        vertical: valueBusiness.value,
      }),

      ...(valueExperience.value !== 'any' && {
        experience: valueExperience.value,
      }),

      ...(valueLearn.value !== 'any' && {
        learn: valueLearn.value,
      }),
    },
  });

  const filteredCourses = filter(courses, {
    fields: {
      ...(valueBusiness.value !== 'any' && {
        vertical: valueBusiness.value,
      }),

      ...(valueExperience.value !== 'any' && {
        experience: valueExperience.value,
      }),

      ...(valueLearn.value !== 'any' && {
        learn: valueLearn.value,
      }),
    },
  });

  const isLoading = loadingCourses || loadingSpecializations;

  const isPureList =
    filteredSpecializations?.length === 0 &&
    filteredCourses?.length === 0 &&
    !isLoading;

  return (
    <CommonLayout>
      <GoBack />
      <section className={s.sectionTitle}>
        <div className="container">
          <Title level={2} weight="bold" className={s.title}>
            Вам подойдёт
          </Title>
        </div>
      </section>

      <SelectBlock />

      <section className={s.content}>
        <div className="container">
          {isPureList ? <Text>Ничего не найдено :(</Text> : null}

          {isLoading ? (
            <Loader />
          ) : (
            <>
              {filteredSpecializations?.length > 0
                ? filteredSpecializations.map((item, index) => (
                    <SpecializationCard
                      title={item?.fields.title}
                      description={item?.fields.description}
                      link={`/specialization/${item?.fields.link}`}
                      peopleCount={item?.fields.studentsCount}
                      courseCount={item?.fields.courseCount}
                      articleCount={item?.fields.articleCount}
                      videoCount={item?.fields.videoCount}
                      index={index}
                      key={item.id}
                      wide
                      className={s.card}
                    />
                  ))
                : null}

              {filteredCourses?.length > 0
                ? filteredCourses.map((cours) => (
                    <EventCard {...cours.fields} key={cours.id} />
                  ))
                : null}
            </>
          )}
        </div>
      </section>
    </CommonLayout>
  );
}

export default Recommendations;
