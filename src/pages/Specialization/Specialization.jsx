import { useEffect, useMemo } from 'react';
import { useParams } from 'react-router-dom';

import Loader from 'components/Loader';

import CommonLayout from 'layouts/CommonLayout';

import CourseContent from 'modules/sections/components/CourseContent';
import YouWillLearn from 'modules/sections/components/YouWillLearn/YouWillLearn';
import PageMainBanner from 'modules/specialization/components/PageMainBanner';
import SuccessfulStories from 'modules/sections/components/SuccessfulStories';
import Benefits from 'modules/sections/components/Benefits';
import Questions from 'modules/sections/components/Questions';
import GoBack from 'modules/sections/components/GoBack';

import useSpecialization from 'modules/sections/hooks/useSpecialization';

import useQuestions from 'modules/specialization/hooks/useQuestions';
import useModules from 'modules/specialization/hooks/useModules';

function Specialization() {
  const { specializationId } = useParams();
  const {
    getSpecializationByLink,
    loadingSpecializations,
    onGetSpecializations,
  } = useSpecialization();

  const { questions } = useQuestions();

  const { modules, onGetModules } = useModules();

  useEffect(() => {
    if (specializationId) {
      onGetModules(specializationId);
    }
  }, [onGetModules, specializationId]);

  useEffect(() => {
    onGetSpecializations();
  }, [onGetSpecializations, specializationId]);

  const specializationData = specializationId
    ? getSpecializationByLink(specializationId)
    : null;

  const fields = specializationData?.fields || null;

  const dataForYouWillLearn = useMemo(() => {
    const arr = [];
    if (fields?.learn_text1) {
      arr.push({
        id: fields?.learn_text1,
        text: fields?.learn_text1,
        img: fields?.learn_picture1 ? fields?.learn_picture1[0].url : '',
      });
    }
    if (fields?.learn_text2) {
      arr.push({
        id: fields?.learn_text2,
        text: fields?.learn_text2,
        img: fields?.learn_picture2 ? fields?.learn_picture2[0].url : '',
      });
    }
    if (fields?.learn_text3) {
      arr.push({
        id: fields?.learn_text3,
        text: fields?.learn_text3,
        img: fields?.learn_picture3 ? fields?.learn_picture3[0].url : '',
      });
    }
    if (fields?.learn_text4) {
      arr.push({
        id: fields?.learn_text4,
        text: fields?.learn_text4,
        img: fields?.learn_picture4 ? fields?.learn_picture4[0].url : '',
      });
    }
    return arr;
  }, [fields]);

  return (
    <CommonLayout>
      {loadingSpecializations ? (
        <Loader fullWidth />
      ) : (
        <>
          <GoBack />
          <PageMainBanner
            title={fields?.title || ''}
            description={fields?.description || ''}
            peopleCount={fields?.studentsCount || ''}
            courseCount={fields?.courseCount || ''}
            materialCount={fields?.materialCount || ''}
            taskCount={fields?.tasksCount || ''}
          />
          <YouWillLearn data={dataForYouWillLearn || []} />
          <CourseContent
            title="Модули"
            description="Название модуля совпадает с&nbsp;категорией, в&nbsp;которой вы размещаете объявления на&nbsp;Авито"
            data={modules}
          />
          <SuccessfulStories />
          <Benefits />
          <Questions data={questions || []} />
        </>
      )}
    </CommonLayout>
  );
}

export default Specialization;
