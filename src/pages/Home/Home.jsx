import { useEffect } from 'react';

import cn from 'classnames';

import { Link } from 'react-router-dom';

import CommonLayout from 'layouts/CommonLayout';

import Button from 'components/Button';

import MainBanner from 'modules/sections/components/MainBanner';
import SelectTraining from 'modules/sections/components/SelectTraining';
import YouCan from 'modules/sections/components/YouCan';
import Specializations from 'modules/sections/components/Specializations/Specializations';
import CoursesMaterials from 'modules/sections/components/CoursesMaterials';
import SuccessfulStories from 'modules/sections/components/SuccessfulStories';
import Benefits from 'modules/sections/components/Benefits';
import Events from 'modules/sections/components/Events';
import CertifiedProfessionals from 'modules/sections/components/CertifiedProfessionals';
import Sponsors from 'modules/sections/components/Sponsors/Sponsors';

import useMaterials from 'modules/sections/hooks/useMaterials';
import useEvents from 'modules/sections/hooks/useEvents';
import useSpecialization from 'modules/sections/hooks/useSpecialization';

import useStickyHeader from 'hooks/useSticky';
import s from './Home.module.css';

function Home() {
  const { materials, onGetMaterials, loadingMaterials } = useMaterials();
  const { events, onGetEvents, loadingEvents } = useEvents();
  const { specializations, onGetSpecializations } = useSpecialization();

  useEffect(() => {
    onGetMaterials();
    onGetEvents(new Date());
    onGetSpecializations();
  }, [onGetMaterials, onGetEvents, onGetSpecializations]);

  const { elemRef, isSticky } = useStickyHeader(false, 0);

  return (
    <CommonLayout>
      <MainBanner />
      <SelectTraining isMobileFooter innerRef={elemRef} />
      <YouCan />
      <Specializations data={specializations} />
      <CoursesMaterials
        events={materials}
        onGetMaterials={onGetMaterials}
        isLoading={loadingMaterials}
      />
      <SuccessfulStories />
      <Benefits />
      <Events
        description="Планируйте обучение заранее"
        events={events}
        onGetEvents={onGetEvents}
        isLoading={loadingEvents}
      />
      <CertifiedProfessionals />
      <Sponsors />
      <div className={cn(s.mobileFooter, isSticky && s.active)}>
        <div className="container">
          <Link to="/recommendations">
            <span>
              <Button fullWidth>Подобрать обучение</Button>
            </span>
          </Link>
        </div>
      </div>
    </CommonLayout>
  );
}

export default Home;
