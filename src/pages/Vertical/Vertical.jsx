import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import filter from 'lodash.filter';

import CommonLayout from 'layouts/CommonLayout';

import SimpleBanner from 'modules/sections/components/SimpleBanner';
import Specializations from 'modules/sections/components/Specializations/Specializations';
import CoursesMaterials from 'modules/sections/components/CoursesMaterials';
import Benefits from 'modules/sections/components/Benefits';
import Events from 'modules/sections/components/Events';
import CertifiedProfessionals from 'modules/sections/components/CertifiedProfessionals';
import Sponsors from 'modules/sections/components/Sponsors/Sponsors';
import GoBack from 'modules/sections/components/GoBack';

import Loader from 'components/Loader';

import useVerticals from 'modules/sections/hooks/useVerticals';
import useMaterials from 'modules/sections/hooks/useMaterials';
import useEvents from 'modules/sections/hooks/useEvents';
import useSpecialization from 'modules/sections/hooks/useSpecialization';

function Vertical() {
  const { verticalId } = useParams();

  const { onGetVerticals, getVerticalByLink, loadingVerticals } =
    useVerticals();

  const { materials, onGetMaterials, loadingMaterials } = useMaterials();

  const { events, onGetEvents, loadingEvents } = useEvents();

  const { specializations, onGetSpecializations } = useSpecialization();

  useEffect(() => {
    onGetVerticals();
  }, [onGetVerticals, verticalId]);

  useEffect(() => {
    onGetMaterials();
  }, [onGetMaterials, verticalId]);

  useEffect(() => {
    onGetEvents();
  }, [onGetEvents, verticalId]);

  useEffect(() => {
    onGetSpecializations();
  }, [onGetSpecializations, verticalId]);

  const verticalData = verticalId ? getVerticalByLink(verticalId) : null;

  const fields = verticalData?.fields || null;

  const filteredMaterials = filter(materials, {
    fields: {
      vertical: verticalId,
    },
  });

  const filteredEvents = filter(events, {
    fields: {
      vertical: verticalId,
    },
  });

  return (
    <CommonLayout>
      {loadingVerticals ? <Loader fullWidth /> : null}
      <GoBack />

      <SimpleBanner
        title={fields?.title || ''}
        description={fields?.description || ''}
        backgroundImg={fields?.background ? fields?.background[0].url : ''}
      />

      <CoursesMaterials
        title="Курсы и материалы"
        description="Быстрые ответы на вопросы и практические советы"
        isFilterWho={false}
        events={filteredMaterials}
        onGetMaterials={onGetMaterials}
        isLoading={loadingMaterials}
      />
      <Specializations data={specializations} />
      <Benefits />
      <Events
        events={filteredEvents}
        onGetEvents={onGetEvents}
        isLoading={loadingEvents}
      />
      <CertifiedProfessionals />
      <Sponsors />
    </CommonLayout>
  );
}

export default Vertical;
