import Title from 'components/Title';

import CommonLayout from 'layouts/CommonLayout';

function Page404() {
  return (
    <CommonLayout>
      <div className="container">
        <Title level={1} weight="bold">
          404
        </Title>
      </div>
    </CommonLayout>
  );
}

export default Page404;
