import { useState, useCallback } from 'react';

import { Recomendation } from 'config/api';

const useRecomendation = () => {
  const [courses, setCourses] = useState();

  const [loadingCourses, setLoadingCourses] = useState(false);

  const onGetCourses = useCallback(async () => {
    setLoadingCourses(true);
    await Recomendation.getCourses()
      .then((resData) => {
        setCourses(resData.records);
        setLoadingCourses(false);
      })

      .catch((errorCode) => {
        // eslint-disable-next-line no-console
        console.log('error', errorCode);
      })

      .finally(() => {
        setLoadingCourses(false);
      });
  }, [setCourses, setLoadingCourses]);

  return {
    onGetCourses,
    courses,
    loadingCourses,
  };
};

export default useRecomendation;
