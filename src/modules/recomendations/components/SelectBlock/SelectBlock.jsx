import { useRef, useState } from 'react';
import ScrollLock from 'react-scrolllock';
import cn from 'classnames';

import Button from 'components/Button';

import SelectTraining from 'modules/sections/components/SelectTraining';

import s from './SelectBlock.module.css';

function SelectBlock() {
  const ref = useRef();

  const [activeFooter, setActiveFooter] = useState(false);

  const handleOpenFooter = () => {
    setActiveFooter(true);
  };

  const handleCloseFooter = () => {
    setActiveFooter(false);
  };
  return (
    <>
      {activeFooter ? (
        <>
          <ScrollLock />
          <div className={s.overlay} onClick={handleCloseFooter} aria-hidden />
        </>
      ) : null}

      <div className={cn(s.footer, activeFooter && s.active)} ref={ref}>
        <div>
          <SelectTraining
            className={s.selectBlock}
            classNameElem={s.selectElem}
            onClick={handleCloseFooter}
          />
        </div>

        <div className={s.buttonWrap}>
          <Button fullWidth onClick={handleOpenFooter} className={s.button}>
            Редактировать
          </Button>
        </div>
      </div>
    </>
  );
}

export default SelectBlock;
