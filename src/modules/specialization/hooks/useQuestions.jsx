import { useState, useCallback, useEffect } from 'react';

import { Questions } from 'config/api';

const useQuestions = () => {
  const [questions, setQuestions] = useState([]);

  const [error, setError] = useState(false);

  const [loadingQuestions, setLoadingQuestions] = useState(false);

  const onGetQuestions = useCallback(async () => {
    setLoadingQuestions(true);
    await Questions.getQuestions()
      .then((resData) => {
        setQuestions(resData.records);
        setLoadingQuestions(false);
      })

      .catch((errorCode) => {
        // eslint-disable-next-line no-console
        console.log('error', errorCode);
        setError(true);
      })

      .finally(() => {
        setLoadingQuestions(false);
      });
  }, [setQuestions, setLoadingQuestions]);

  useEffect(() => {
    onGetQuestions();
  }, [onGetQuestions]);

  return {
    onGetQuestions,
    questions,
    error,
    loadingQuestions,
  };
};

export default useQuestions;
