import { useState, useCallback, useEffect } from 'react';

import { Modules } from 'config/api';

const useModules = () => {
  const [modules, setModuless] = useState([]);

  const [error, setError] = useState(false);

  const [loadingModules, setLoadingModules] = useState(false);

  const onGetModules = useCallback(
    async (type) => {
      if (type) {
        setLoadingModules(true);
        await Modules.getModules(type)
          .then((resData) => {
            setModuless(resData.records);
            setLoadingModules(false);
          })

          .catch((errorCode) => {
            // eslint-disable-next-line no-console
            console.log('error', errorCode);
            setError(true);
          })

          .finally(() => {
            setLoadingModules(false);
          });
      }
    },
    [setModuless, setLoadingModules],
  );

  useEffect(() => {
    onGetModules();
  }, [onGetModules]);

  return {
    onGetModules,
    modules,
    error,
    loadingModules,
  };
};

export default useModules;
