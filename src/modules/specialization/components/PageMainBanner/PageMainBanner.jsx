import { useMemo } from 'react';

import PropTypes from 'prop-types';

import CardBanner from 'modules/home/components/CardBanner';
import backgroundImg from 'assets/images/background/program1.png';

import { BANNER_CATEGORIES } from './constants';

import s from './PageMainBanner.module.css';

function PageMainBanner({
  title,
  description,
  peopleCount,
  courseCount,
  materialCount,
  taskCount,
}) {
  const contentArray = useMemo(() => {
    const arr = [];
    if (courseCount) {
      arr.push({
        id: 'course',
        category: 'course',
        count: courseCount,
      });
    }
    if (materialCount) {
      arr.push({
        id: 'materials',
        category: 'materials',
        count: materialCount,
      });
    }
    if (taskCount) {
      arr.push({
        id: 'exercise',
        category: 'exercise',
        count: taskCount,
      });
    }
    return arr;
  }, [courseCount, materialCount, taskCount]);
  return (
    <section className={s.root}>
      <div className="container">
        <CardBanner
          title={title}
          description={description}
          categories={BANNER_CATEGORIES}
          peopleCount={peopleCount}
          contentArray={contentArray}
          backgroundImg={backgroundImg}
        />
      </div>
    </section>
  );
}

PageMainBanner.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  peopleCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  courseCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  materialCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  taskCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

PageMainBanner.defaultProps = {
  title: '',
  description: '',
  peopleCount: 0,
  courseCount: null,
  materialCount: null,
  taskCount: null,
};

PageMainBanner.displayName = 'PageMainBanner';

export default PageMainBanner;
