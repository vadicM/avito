import specializationImage from 'assets/icons/categories/specialization.svg';

export const BANNER_CATEGORIES = [
  {
    id: '1',
    title: 'Специализация',
    img: specializationImage,
  },
];

export const BANNER_CONTENT_ARRAY = [
  {
    id: '11',
    category: 'topic',
    count: 2,
  },
  {
    id: '22',
    category: 'materials',
    count: 12,
  },
  {
    id: '33',
    category: 'exercise',
    count: 12,
  },
];
