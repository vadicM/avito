import { useState, useCallback } from 'react';

import { MainPage } from 'config/api';

const useMainBanner = () => {
  const [mainBanner, setMainBanner] = useState([]);

  const [error, setError] = useState(false);

  const [loadingMainBanner, setLoadingMainBanner] = useState(false);

  const onGetMainBanner = useCallback(async () => {
    setLoadingMainBanner(true);
    await MainPage.getBannerTable()
      .then((resData) => {
        setMainBanner(resData.records[0].fields);
        setLoadingMainBanner(false);
      })

      .catch((errorCode) => {
        // eslint-disable-next-line no-console
        console.log('error', errorCode);
        setError(true);
      })

      .finally(() => {
        setLoadingMainBanner(false);
      });
  }, [setMainBanner, setLoadingMainBanner]);

  return { onGetMainBanner, mainBanner, error, loadingMainBanner };
};

export default useMainBanner;
