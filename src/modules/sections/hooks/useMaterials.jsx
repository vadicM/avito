import { useState, useCallback } from 'react';

import { Materials } from 'config/api';

const useMaterials = () => {
  const [materials, setMaterials] = useState([]);

  const [error, setError] = useState(false);

  const [loadingMaterials, setLoadingMaterials] = useState(false);

  const onGetMaterials = useCallback(
    async (type, vertical) => {
      setLoadingMaterials(true);
      await Materials.getMaterials(type, vertical)
        .then((resData) => {
          setMaterials(resData.records);
          setLoadingMaterials(false);
        })

        .catch((errorCode) => {
          // eslint-disable-next-line no-console
          console.log('error', errorCode);
          setError(true);
        })

        .finally(() => {
          setLoadingMaterials(false);
        });
    },
    [setMaterials, setLoadingMaterials],
  );

  return {
    onGetMaterials,
    materials,
    error,
    loadingMaterials,
  };
};

export default useMaterials;
