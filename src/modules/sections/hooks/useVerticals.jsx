import { useState, useCallback, useEffect } from 'react';

import { Verticals } from 'config/api';

const useVerticals = () => {
  const [verticals, setVerticals] = useState([]);

  const [error, setError] = useState(false);

  const [loadingVerticals, setLoadingVerticals] = useState(false);

  const onGetVerticals = useCallback(async () => {
    setLoadingVerticals(true);
    await Verticals.getVerticals()
      .then((resData) => {
        setVerticals(resData.records);
        setLoadingVerticals(false);
      })

      .catch((errorCode) => {
        // eslint-disable-next-line no-console
        console.log('error', errorCode);
        setError(true);
      })

      .finally(() => {
        setLoadingVerticals(false);
      });
  }, [setVerticals, setLoadingVerticals]);

  const getVerticalByLink = useCallback(
    (link) => {
      if (verticals?.length > 0) {
        const findedIndex = verticals.findIndex(
          ({ fields }) => fields.link === link,
        );
        return findedIndex > -1 ? verticals[findedIndex] : null;
      }
      return null;
    },
    [verticals],
  );

  useEffect(() => {
    onGetVerticals();
  }, [onGetVerticals]);

  return {
    onGetVerticals,
    getVerticalByLink,
    verticals,
    error,
    loadingVerticals,
  };
};

export default useVerticals;
