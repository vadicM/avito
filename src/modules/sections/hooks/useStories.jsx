import { useState, useCallback } from 'react';

import { Stories } from 'config/api';

const useStories = () => {
  const [stories, setStories] = useState([]);

  const [error, setError] = useState(false);

  const [loadingStories, setLoadingStories] = useState(false);

  const onGetStories = useCallback(async () => {
    setLoadingStories(true);
    await Stories.getStories()
      .then((resData) => {
        setStories(resData.records);
        setLoadingStories(false);
      })

      .catch((errorCode) => {
        // eslint-disable-next-line no-console
        console.log('error', errorCode);
        setError(true);
      })

      .finally(() => {
        setLoadingStories(false);
      });
  }, [setStories, setLoadingStories]);

  return {
    onGetStories,
    stories,
    error,
    loadingStories,
  };
};

export default useStories;
