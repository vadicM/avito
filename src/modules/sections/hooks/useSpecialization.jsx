import { useState, useCallback, useEffect } from 'react';

import { Specializations } from 'config/api';

const useSpecialization = () => {
  const [specializations, setSpecializations] = useState([]);

  const [error, setError] = useState(false);

  const [loadingSpecializations, setLoadingSpecializations] = useState(false);

  const onGetSpecializations = useCallback(async () => {
    setLoadingSpecializations(true);
    await Specializations.getSpecializations()
      .then((resData) => {
        setSpecializations(resData.records);
        setLoadingSpecializations(false);
      })

      .catch((errorCode) => {
        // eslint-disable-next-line no-console
        console.log('error', errorCode);
        setError(true);
      })

      .finally(() => {
        setLoadingSpecializations(false);
      });
  }, [setSpecializations, setLoadingSpecializations]);

  useEffect(() => {
    onGetSpecializations();
  }, [onGetSpecializations]);

  const getSpecializationByLink = useCallback(
    (link) => {
      if (specializations?.length > 0) {
        const findedIndex = specializations.findIndex(
          ({ fields }) => fields.link === link,
        );
        return findedIndex > -1 ? specializations[findedIndex] : null;
      }
      return null;
    },
    [specializations],
  );

  return {
    onGetSpecializations,
    specializations,
    error,
    loadingSpecializations,
    getSpecializationByLink,
  };
};

export default useSpecialization;
