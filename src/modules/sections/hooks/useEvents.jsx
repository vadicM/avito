import { useState, useCallback } from 'react';

import { Events } from 'config/api';
import { formatDate } from 'utils/formatDate';

const getDatePlusMinusDate = (dateParam, isPlus) => {
  const date = new Date(dateParam);
  if (isPlus) {
    date.setDate(date.getDate() + 1);
  } else {
    date.setDate(date.getDate() - 1);
  }

  return date;
};

const useEvents = () => {
  const [events, setEvents] = useState([]);

  const [error, setError] = useState(false);

  const [loadingEvents, setLoadingEvents] = useState(false);

  const onGetEvents = useCallback(
    async (date) => {
      const datePlusDay = formatDate(getDatePlusMinusDate(date, true));
      const dateMinusDay = formatDate(getDatePlusMinusDate(date, false));
      setLoadingEvents(true);
      await Events.getEvents(dateMinusDay, datePlusDay)
        .then((resData) => {
          setEvents(resData.records);
          setLoadingEvents(false);
        })

        .catch((errorCode) => {
          // eslint-disable-next-line no-console
          console.log('error', errorCode);
          setError(true);
        })

        .finally(() => {
          setLoadingEvents(false);
        });
    },
    [setEvents, setLoadingEvents],
  );

  return {
    onGetEvents,
    events,
    error,
    loadingEvents,
  };
};

export default useEvents;
