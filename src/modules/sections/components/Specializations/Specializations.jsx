import PropTypes from 'prop-types';

import Text from 'components/Text';
import Title from 'components/Title';
import SpecializationCard from 'modules/home/components/SpecializationCard';

import s from './Specializations.module.css';

function Specializations({ data }) {
  return (
    <section className={s.root}>
      <div className="container">
        <Title level={2} weight="bold" className={s.title}>
          Специализации
        </Title>
        <Text className={s.description} size="large">
          Курсы, вебинары и статьи, объединённые одной темой
        </Text>
        <div className={s.row}>
          {data?.length > 0
            ? data.map((item, index) => (
                <SpecializationCard
                  title={item?.fields.title}
                  description={item?.fields.description}
                  link={`/specialization/${item?.fields.link}`}
                  peopleCount={item?.fields.studentsCount}
                  courseCount={item?.fields.courseCount}
                  articleCount={item?.fields.articleCount}
                  videoCount={item?.fields.videoCount}
                  index={index}
                  key={item.id}
                />
              ))
            : null}
        </div>
      </div>
    </section>
  );
}

Specializations.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      fields: PropTypes.objectOf,
    }),
  ),
};

Specializations.defaultProps = {
  data: [],
};

Specializations.displayName = 'Specializations';

export default Specializations;
