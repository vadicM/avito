/* eslint-disable import/prefer-default-export */

import { ReactComponent as SkillboxImg } from 'assets/images/sponsors/skillbox.svg';
import { ReactComponent as NetologiaImg } from 'assets/images/sponsors/netologia.svg';
import { ReactComponent as YandexImg } from 'assets/images/sponsors/yandex.svg';
import { ReactComponent as SkillsetterImg } from 'assets/images/sponsors/skillsetter.svg';

export const SPONSORS_ARRAY = [
  {
    id: '1',
    title: 'Netology',
    link: 'https://netology.ru/',
    image: <NetologiaImg />,
  },
  {
    id: '2',
    title: 'Skillbox',
    link: 'https://skillbox.ru/',
    image: <SkillboxImg />,
  },
  {
    id: '3',
    title: 'Yandex Practicum',
    link: 'https://practicum.yandex.ru/',
    image: <YandexImg />,
  },
  {
    id: '4',
    title: 'Skillsetter',
    link: 'https://skillsetter.io/',
    image: <SkillsetterImg />,
  },
];
