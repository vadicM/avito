import Title from 'components/Title';

import { SPONSORS_ARRAY } from './constants';

import s from './Sponsors.module.css';

function Sponsors() {
  return (
    <section className={s.root}>
      <div className="container">
        <Title level={6} className={s.title}>
          Образовательные партнеры, которые учат работать на Авито
        </Title>
        <ul className={s.list}>
          {SPONSORS_ARRAY.map(({ id, title, image, link }) => (
            <li key={id} className={s.listItem}>
              <a
                href={link}
                title={title}
                target="_blank"
                rel="noopener noreferrer"
                className={s.link}>
                {image}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
}

export default Sponsors;
