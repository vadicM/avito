import { useNavigate } from 'react-router-dom';

import Button from 'components/Button';
import { ArrowBackIcon } from 'components/Icons';
import Text from 'components/Text';

import s from './GoBack.module.css';

function GoBack() {
  const navigate = useNavigate();

  const handleCloseModal = () => {
    navigate(-1);
  };

  return (
    <div className={s.root}>
      <div className="container">
        <Button type="ghost" className={s.button} onClick={handleCloseModal}>
          <ArrowBackIcon />
          <Text className={s.text}>Назад</Text>
        </Button>
      </div>
    </div>
  );
}

export default GoBack;
