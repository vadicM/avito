import Button from 'components/Button';
import Text from 'components/Text';
import Title from 'components/Title';

import professionalsImg from 'assets/images/people/professionals.svg';

import s from './CertifiedProfessionals.module.css';

function CertifiedProfessionals() {
  return (
    <section className={s.root}>
      <div className="container">
        <div className={s.row}>
          <div className={s.cell}>
            <Text className={s.subTitle} size="large" muted>
              Если на учёбу нет времени
            </Text>

            <Title level={2} weight="bold" className={s.title}>
              Сертифицированные авитологи
            </Title>

            <Text className={s.description} size="large">
              Помогают предпринимателям увеличить продажи.
              <br />
              Мы тщательно проверили их знания и опыт — <br />
              как если бы сами хотели взять их в подрядчики
            </Text>

            <Button size="large" className={s.button}>
              <span>
                <a href="https://www.avito.ru/business/avitolog">
                  Выбрать специалиста
                </a>
              </span>
            </Button>
          </div>
          <div className={s.imgBlock}>
            <img src={professionalsImg} alt="Professionals" />
          </div>
        </div>
      </div>
    </section>
  );
}

export default CertifiedProfessionals;
