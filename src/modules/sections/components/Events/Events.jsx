import PropTypes from 'prop-types';

import { useState, useMemo } from 'react';

import Text from 'components/Text';
import Title from 'components/Title';
import Button from 'components/Button';
import EventCard from 'modules/home/components/EventCard';

import CalendarCard from 'modules/home/components/CalendarCard/CalendarCard';
import Carousel from 'components/Carousel';

import useViewport from 'hooks/useViewport';

import { getDayName, getDayNumber } from 'utils/dates';

import Loader from 'components/Loader';
import s from './Events.module.css';

import {
  DAYS_ARRAY,
  EVENTS_CALENDAR_ARRAY,
  PREVIOUS_DAYS,
  EVENTS_PURE_ARRAY,
} from './constants';

function Events({ title, description, events, onGetEvents, isLoading }) {
  const { isMobile } = useViewport();

  const [activeIndex, setActiveIndex] = useState(PREVIOUS_DAYS);

  const [isShowMore, setIsShowMore] = useState(false);

  const handleToggleShowMore = () => {
    setIsShowMore((prevState) => !prevState);
  };

  const eventsArray = useMemo(
    () => (isShowMore && events?.length > 3 ? events : events.slice(0, 3)),
    [events, isShowMore],
  );

  const handleGetEventByDate = (date, index) => {
    onGetEvents(date);
    setActiveIndex(index);
  };

  return (
    <section className={s.root}>
      <div className="container">
        <div className={s.row}>
          <aside className={s.sidebar}>
            <Title level={2} weight="bold" className={s.title}>
              {title}
            </Title>
            {description ? (
              <Text className={s.description} size="large">
                Планируйте обучение заранее
              </Text>
            ) : null}
          </aside>
          <div className={s.cell}>
            <div className={s.carousel}>
              <Carousel
                breakpoints={{
                  320: {
                    slidesPerView: 7,
                    spaceBetween: 4,
                  },
                  680: {
                    slidesPerView: 9,
                  },
                  992: {
                    slidesPerView: 7,
                    spaceBetween: 12,
                  },
                }}
                navigation={!isMobile}
                slidesPerGroup={3}
                initialSlide={isMobile ? 4 : PREVIOUS_DAYS}
                freeMode
                mousewheel
                className={s.carouselItem}
                arrowType="square">
                {DAYS_ARRAY.map((day, index) => (
                  <CalendarCard
                    dayNumber={getDayNumber(new Date(day))}
                    day={getDayName(new Date(day))}
                    onClick={() => handleGetEventByDate(day, index)}
                    active={activeIndex === index}
                    key={day}
                  />
                ))}
              </Carousel>
            </div>

            {isLoading ? (
              <Loader />
            ) : (
              <>
                {events?.length > 0
                  ? eventsArray.map((event) => (
                      <EventCard
                        key={event.id}
                        title={event?.fields?.title}
                        description={event?.fields?.description}
                        type={event?.fields?.type}
                        date={event?.fields?.date}
                        link={event?.fields?.link}
                        isDate
                      />
                    ))
                  : EVENTS_PURE_ARRAY.map((event) => (
                      // eslint-disable-next-line react/jsx-props-no-spreading
                      <EventCard key={event.id} {...event} disabled />
                    ))}

                {events?.length > 3 ? (
                  <Button
                    size="extra-large"
                    type="bordered"
                    fullWidth
                    onClick={handleToggleShowMore}
                    className={s.button}>
                    {isShowMore ? 'Свернуть' : 'Показать ещё'}
                  </Button>
                ) : null}
              </>
            )}
          </div>
        </div>
      </div>
    </section>
  );
}

Events.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  className: PropTypes.string,
  events: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      description: PropTypes.string,
      link: PropTypes.string,
      imageBgr: PropTypes.string,
      categories: PropTypes.arrayOf,
    }),
  ),
  onGetEvents: PropTypes.func,
  isLoading: PropTypes.bool,
};

Events.defaultProps = {
  title: 'Мероприятия',
  description: '',
  className: null,
  events: EVENTS_CALENDAR_ARRAY,
  onGetEvents: null,
  isLoading: false,
};

Events.displayName = 'Events';

export default Events;
