import { ReactComponent as BookmarkImage } from 'assets/icons/categories/bookmark.svg';
import calendarImage from 'assets/icons/categories/calendar.svg';
import timeImage from 'assets/icons/categories/time.svg';

export const EVENTS_PURE_ARRAY = [
  {
    id: '1',
    title: 'Мероприятий пока нет',
    link: '#',
    categories: [
      {
        id: '1',
        title: 'Но всё может измениться — следите за обновлениями!',
        img: timeImage,
      },
    ],
  },
];

export const EVENTS_CALENDAR_ARRAY = [
  {
    id: '1',
    title: 'Разбираем услуги продвижения объявлений на Авито',
    link: '/',
    categories: [
      {
        id: '1',
        title: '3 февраля, 13:30 – 16:30',
        img: calendarImage,
      },
      {
        id: '2',
        title: 'Работодателям',
        link: '#',
        img: <BookmarkImage />,
      },
    ],
  },
  {
    id: '2',
    title:
      'Кабинет Авито Pro и как его использовать профессионалам рынка недвижимости',
    link: '/',
    categories: [
      {
        id: '1',
        title: '3 февраля, 13:30 – 16:30',
        img: calendarImage,
      },
      {
        id: '2',
        title: 'Профессионалам недвижимости',
        link: '#',
        img: <BookmarkImage />,
      },
    ],
  },
  {
    id: '3',
    title: 'Как оформить профиль, чтобы он привлёк заказчиков',
    link: '/',
    categories: [
      {
        id: '1',
        title: '3 февраля, 13:30 – 16:30',
        img: calendarImage,
      },
      {
        id: '2',
        title: 'Тем, кто оказывает услуги',
        link: '#',
        img: <BookmarkImage />,
      },
    ],
  },
];

export const PREVIOUS_DAYS = 6;

function getDateArray(start, end) {
  // eslint-disable-next-line no-array-constructor
  const arr = new Array();
  const dt = new Date(start);
  while (dt <= end) {
    arr.push(new Date(dt));
    dt.setDate(dt.getDate() + 1);
  }
  return arr;
}

function addDays(theDate, days) {
  return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}

const startDate = addDays(new Date(), -PREVIOUS_DAYS);
const endDate = addDays(new Date(), 14);

export const DAYS_ARRAY = getDateArray(startDate, endDate);
