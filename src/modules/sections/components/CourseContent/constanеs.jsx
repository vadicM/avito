/* eslint-disable import/prefer-default-export */
import icon1 from 'assets/icons/goods/notes.svg';
import icon2 from 'assets/icons/goods/school.svg';
import icon3 from 'assets/icons/goods/review.svg';

export const CONTENT_ARRAY = [
  {
    id: '1',
    title: 'Товары',
    content: [
      {
        id: '01',
        name: 'Курс «Путь клиента. Правила продаж на Авито»',
        link: '#',
        icon: icon1,
      },
      {
        id: '02',
        name: 'Для тех, кто продаёт с доставкой: как защититься от мошенников',
        link: '#',
        icon: icon2,
      },
      {
        id: '03',
        name: 'Как работают отзывы и формируется рейтинг продавца на Авито',
        link: '#',
        icon: icon2,
      },
      {
        id: '04',
        name: 'От трёх комплектов гантелей на Авито до трёх магазинов: история предпринимателя из Калининграда',
        link: '#',
        icon: icon3,
      },
    ],
  },
  {
    id: '2',
    title: 'Услуги',
    content: [
      {
        id: '21',
        name: 'Курс «Путь клиента. Правила продаж на Авито»',
        link: '#',
        icon: icon1,
      },
    ],
  },
  {
    id: '3',
    title: 'Работа',
    content: [
      {
        id: '31',
        name: 'Курс «Путь клиента. Правила продаж на Авито»',
        link: '#',
        icon: icon1,
      },
    ],
  },
  {
    id: '4',
    title: 'Авто',
    content: [
      {
        id: '41',
        name: 'Курс «Путь клиента. Правила продаж на Авито»',
        link: '#',
        icon: icon1,
      },
    ],
  },
  {
    id: '5',
    title: 'Недвижимость',
    content: [
      {
        id: '51',
        name: 'Курс «Путь клиента. Правила продаж на Авито»',
        link: '#',
        icon: icon1,
      },
    ],
  },
];
