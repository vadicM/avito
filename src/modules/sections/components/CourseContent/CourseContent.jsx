/* eslint-disable react/no-danger */
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';
import cn from 'classnames';
import TabElement from 'components/TabElement';
import Text from 'components/Text';
import Title from 'components/Title';

// import { CONTENT_ARRAY } from './constanеs';

import icon1 from 'assets/icons/goods/notes.svg';
import icon2 from 'assets/icons/goods/school.svg';
import icon3 from 'assets/icons/goods/review.svg';

import s from './CourseContent.module.css';

const getIcon = (indexParam) => {
  if (1 % indexParam === 0) {
    return icon1;
  }
  if (2 % indexParam === 0) {
    return icon2;
  }
  if (3 % indexParam === 0) {
    return icon3;
  }
  return icon1;
};

function CourseContent({ title, description, data, className }) {
  return (
    <section className={cn(s.root, className)}>
      <div className="container">
        <div className={s.row}>
          <aside className={s.aside}>
            <Title level={2} weight="bold" className={s.title}>
              {title}
            </Title>

            <Text size="large" className={s.description}>
              <span dangerouslySetInnerHTML={{ __html: description }} />
            </Text>
          </aside>

          <div className={s.cell}>
            {data?.length > 0
              ? data.map(({ fields, id }) => (
                  <TabElement title={fields.title} key={id} shadow>
                    {fields?.content?.length > 0 ? (
                      <ul className={s.list}>
                        {fields.content.map((contentItem, index) => (
                          <li key={contentItem} className={s.listItem}>
                            <a
                              href={
                                fields?.links?.[index]
                                  ? fields.links[index]
                                  : '#'
                              }
                              className={s.link}>
                              <img
                                src={getIcon(index + 1)}
                                alt="icon"
                                className={s.icon}
                              />

                              <Text className={s.text}>{contentItem}</Text>
                            </a>
                          </li>
                        ))}
                      </ul>
                    ) : null}
                  </TabElement>
                ))
              : null}
          </div>
        </div>
      </div>
    </section>
  );
}

CourseContent.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      fields: PropTypes.objectOf,
    }),
  ),
};

CourseContent.defaultProps = {
  className: null,
  title: '',
  description: '',
  data: [],
};

CourseContent.displayName = 'CourseContent';

export default CourseContent;
