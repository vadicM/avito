/* eslint-disable import/prefer-default-export */
import authorImg1 from 'assets/images/people/author1.jpg';
import authorImg2 from 'assets/images/people/author2.jpg';

import bgrImg1 from 'assets/images/background/story1.png';
import bgrImg2 from 'assets/images/background/story2.png';
import bgrImg3 from 'assets/images/background/story3.jpg';

export const STORIES_ARRAY = [
  {
    id: '1',
    title: 'Увеличил продажи',
    bigTitle: 'на 200%',
    type: 'text',
    backgroundImg: bgrImg1,
    author: {
      img: authorImg1,
      name: 'Татьяна Тимченко',
      category: 'Товары · Цветочный магазин',
    },
  },
  {
    id: '2',
    type: 'video',
    backgroundImg: bgrImg3,
    author: {
      name: 'Евгения Вихристенко',
      category: 'Услуги · Сервис',
    },
  },
  {
    id: '3',
    title: 'Короткая цитата в 30–40 символов из статьи',
    type: 'text',
    backgroundImg: bgrImg2,
    author: {
      img: authorImg2,
      name: 'Евгения Вихристенко',
      category: 'Услуги · Сервис',
    },
  },

  {
    id: '4',
    type: 'video',
    backgroundImg: bgrImg3,
    author: {
      name: 'Евгения Вихристенко',
      category: 'Услуги · Сервис',
    },
  },
];
