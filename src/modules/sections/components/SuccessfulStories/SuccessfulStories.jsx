import Carousel from 'components/Carousel';
import Title from 'components/Title';

import HistoryCard from 'modules/home/components/HistoryCard';

import useViewport from 'hooks/useViewport';

import useStories from 'modules/sections/hooks/useStories';
import { useEffect } from 'react';
import { STORIES_ARRAY } from './constants';

import s from './SuccessfulStories.module.css';

function SuccessfulStories() {
  const { isMobile } = useViewport();

  const { stories = STORIES_ARRAY, onGetStories } = useStories();

  useEffect(() => {
    onGetStories();
  }, [onGetStories]);

  return (
    <section className={s.root}>
      <div className="container">
        <Title level={2} weight="bold" className={s.title}>
          Истории успеха
        </Title>
        {stories?.length > 0 ? (
          <div className={s.carousel}>
            <Carousel
              slidesPerView={isMobile ? 1 : 3}
              navigation={!isMobile}
              spaceBetween={isMobile ? 12 : 24}
              className={s.carouselItem}>
              {stories.map((story, index) => (
                <HistoryCard
                  key={story.id}
                  title={story?.fields?.title}
                  bigTitle={story?.fields?.bigTitle || ''}
                  author={{
                    name: story?.fields?.authorName,
                    img: story?.fields?.authorAvatar
                      ? story?.fields?.authorAvatar[0].url
                      : '',
                    category: story?.fields?.categories,
                  }}
                  type={story?.fields?.type}
                  link={story?.fields?.link}
                  backgroundImg={
                    story?.fields?.picture ? story?.fields?.picture[0].url : ''
                  }
                  index={index}
                />
              ))}
            </Carousel>
          </div>
        ) : null}
      </div>
    </section>
  );
}

export default SuccessfulStories;
