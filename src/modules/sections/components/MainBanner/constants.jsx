/* eslint-disable import/prefer-default-export */
import bannerPic1 from 'assets/images/slider/pic1.jpg';
import bannerPic2 from 'assets/images/slider/pic2.jpg';
import bannerPic3 from 'assets/images/slider/pic3.jpg';

export const MAIN_BANNER_ARRAY = [
  {
    id: '1',
    image: bannerPic1,
  },
  {
    id: '2',
    image: bannerPic2,
  },
  {
    id: '3',
    image: bannerPic3,
  },
];
