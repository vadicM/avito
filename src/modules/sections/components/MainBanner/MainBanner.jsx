import { useEffect, useMemo } from 'react';

import Title from 'components/Title';
import Text from 'components/Text';

import Carousel from 'components/Carousel';
import useMainBanner from 'modules/sections/hooks/useMainBanner';

import Loader from 'components/Loader';

import s from './MainBanner.module.css';

function MainBanner() {
  const { mainBanner, onGetMainBanner, loadingMainBanner } = useMainBanner();

  useEffect(() => {
    onGetMainBanner();
  }, [onGetMainBanner]);

  const sliderArray = useMemo(() => {
    const arr = [];

    if (mainBanner?.photo1) {
      arr.push(mainBanner.photo1[0]);
    }

    if (mainBanner?.photo2) {
      arr.push(mainBanner.photo2[0]);
    }

    if (mainBanner?.photo3) {
      arr.push(mainBanner.photo3[0]);
    }

    if (mainBanner?.photo4) {
      arr.push(mainBanner.photo4[0]);
    }

    if (mainBanner?.photo5) {
      arr.push(mainBanner.photo5[0]);
    }

    return arr;
  }, [mainBanner]);

  if (loadingMainBanner) {
    return <Loader className={s.loader} />;
  }

  return (
    <section className={s.root}>
      <div className="container">
        <div className={s.banner}>
          {sliderArray.length > 0 ? (
            <div className={s.carousel}>
              <Carousel
                spaceBetween={0}
                slidesPerView={1}
                autoplay={{
                  delay: 5000,
                  disableOnInteraction: false,
                }}
                effect="fade">
                {sliderArray.map(({ id, url }) => (
                  <div key={id} className={s.carouselItem}>
                    <img src={url} alt="banner" className={s.image} />
                  </div>
                ))}
              </Carousel>
            </div>
          ) : null}

          <div className={s.content}>
            {mainBanner?.title ? (
              <Title level={1} weight="bold" className={s.title}>
                {mainBanner.title}
              </Title>
            ) : null}

            {mainBanner?.subtitle ? (
              <Text size="large" className={s.description}>
                {mainBanner.subtitle}
              </Text>
            ) : null}

            <div className={s.row}>
              <div className={s.cell}>
                <Title level={4} weight="bold" className={s.subTitle}>
                  Каждый второй
                </Title>
                <Text className={s.text}>бизнес развивается на Авито</Text>
              </div>
              <div className={s.cell}>
                <Title level={4} weight="bold" className={s.subTitle}>
                  40 000
                </Title>
                <Text className={s.text}>уже начали учиться</Text>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export default MainBanner;
