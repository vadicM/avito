import { useState, useCallback, useEffect } from 'react';

import PropTypes from 'prop-types';

import cn from 'classnames';

import Title from 'components/Title';
import Text from 'components/Text';

import bannerPic from 'assets/images/slider/pic4.jpg';
import verticalIcon1 from 'assets/icons/verticals/pic1.svg';

import s from './SimpleBanner.module.css';

const SCROLL_HEIGHT = 250;

function SimpleBanner({ title, description, backgroundImg, className }) {
  const [isMenuActive, setIsMenuActive] = useState(false);

  const handleScroll = useCallback(() => {
    const scrollTopHeight = window.scrollY;

    if (scrollTopHeight > SCROLL_HEIGHT) {
      if (!isMenuActive) {
        setIsMenuActive(true);
      }
    } else if (isMenuActive) {
      setIsMenuActive(false);
    }
  }, [isMenuActive]);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);

  return (
    <>
      <div className={cn(s.subMenu, isMenuActive && s.active)}>
        <div className="container">
          <div className={s.subContent}>
            <img src={verticalIcon1} alt="icon" width={26} height={26} />
            <Text className={s.subText}>{title}</Text>
          </div>
        </div>
      </div>
      <section className={cn(s.root, className)}>
        <div className="container">
          <div className={s.block}>
            {backgroundImg ? (
              <img
                src={backgroundImg}
                alt="background"
                className={s.backgroundImg}
              />
            ) : null}

            <div className={s.content}>
              {title ? (
                <Title level={1} className={s.title} weight="bold">
                  {title}
                </Title>
              ) : null}

              {description ? (
                <Text className={s.text} size="large">
                  {description}
                </Text>
              ) : null}
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

SimpleBanner.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  backgroundImg: PropTypes.string,
  className: PropTypes.string,
};

SimpleBanner.defaultProps = {
  title: '',
  description: '',
  backgroundImg: bannerPic,
  className: null,
};

SimpleBanner.displayName = 'SimpleBanner';

export default SimpleBanner;
