import Text from 'components/Text';
import Title from 'components/Title';
import s from './Benefits.module.css';
import { BENEFITS_ARRAY } from './constants';

function Benefits() {
  return (
    <section className={s.root}>
      <div className="container">
        <Title level={2} weight="bold" className={s.title}>
          Преимущества школы
        </Title>
        <ul className={s.list}>
          {BENEFITS_ARRAY.map(({ title, img, id }) => (
            <li key={id} className={s.listItem}>
              <div className={s.imageWrapper}>
                <img src={img} alt={title || 'image'} className={s.image} />
              </div>
              <Text className={s.text} size="large">
                {title}
              </Text>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
}

export default Benefits;
