/* eslint-disable import/prefer-default-export */

import pic1 from 'assets/images/benefits/pic1.svg';
import pic2 from 'assets/images/benefits/pic2.svg';
import pic3 from 'assets/images/benefits/pic3.svg';
import pic4 from 'assets/images/benefits/pic4.svg';

export const BENEFITS_ARRAY = [
  {
    id: '1',
    title: 'Преподаватели \nс опытом в бизнесе',
    img: pic1,
  },
  {
    id: '2',
    title: 'Бесплатное \nобучение',
    img: pic2,
  },
  {
    id: '3',
    title: 'Практика \nво время урока',
    img: pic3,
  },
  {
    id: '4',
    title: 'Обратная связь \nот опытных авитологов ',
    img: pic4,
  },
];
