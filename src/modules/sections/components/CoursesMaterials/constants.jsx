export const VERTICAL_ARRAY = [
  {
    id: '1',
    title: 'Для всех',
    value: null,
  },
  {
    id: '2',
    title: 'Продавцам транспорта',
    value: 'transport',
  },
  {
    id: '3',
    title: 'Работодателям',
    value: 'employer',
  },
  {
    id: '4',
    title: 'Профессионалам недвижимости',
    value: 'realestate',
  },
  {
    id: '5',
    title: 'Тем, кто продаёт товары',
    value: 'goods',
  },
  {
    id: '6',
    title: 'Тем, кто оказывает услуги',
    value: 'services',
  },
];

export const TYPE_ARRAY = [
  {
    id: '1',
    title: 'Любой',
    value: null,
  },
  {
    id: '2',
    title: 'Курсы',
    value: 'course',
  },
  {
    id: '3',
    title: 'Статьи',
    value: 'article',
  },
  {
    id: '4',
    title: 'Видео',
    value: 'video',
  },
];
