import { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';

import Select from 'components/Select';
import Text from 'components/Text';
import Title from 'components/Title';
import Button from 'components/Button';
import EventCard from 'modules/home/components/EventCard';

import ListSelectItems from 'modules/home/components/ListSelectItems/ListSelectItems';

import Loader from 'components/Loader';
import s from './CoursesMaterials.module.css';

import { TYPE_ARRAY, VERTICAL_ARRAY } from './constants';

function CoursesMaterials({
  title,
  description,
  className,
  isFilterWho,
  isFilterType,
  events,
  onGetMaterials,
  isLoading,
}) {
  const [isShowMore, setIsShowMore] = useState(false);
  const handleToggleShowMore = () => {
    setIsShowMore((prevState) => !prevState);
  };
  const eventsArray = useMemo(
    () => (isShowMore && events?.length > 3 ? events : events.slice(0, 3)),
    [events, isShowMore],
  );

  const [filterType, setFilterType] = useState(TYPE_ARRAY[0]);
  const [filterVertical, setFilterVertical] = useState(VERTICAL_ARRAY[0]);

  useEffect(() => {
    if (onGetMaterials) {
      onGetMaterials(filterType?.value, filterVertical?.value);
    }
  }, [filterType, filterVertical, onGetMaterials]);

  return (
    <section className={s.root}>
      <div className="container">
        <Title level={2} weight="bold" className={s.title}>
          {title}
        </Title>

        <Text className={s.description} size="large">
          {description}
        </Text>

        <div className={s.row}>
          <div className={s.sidebar}>
            {isFilterWho ? (
              <div className={s.block}>
                <div className={s.desktop}>
                  <Title level={5} weight="bold" className={s.subTitle}>
                    Для кого
                  </Title>

                  <ListSelectItems
                    data={VERTICAL_ARRAY}
                    onChange={setFilterVertical}
                  />
                </div>

                <div className={s.tablet}>
                  <Select
                    options={VERTICAL_ARRAY}
                    value={filterVertical}
                    label="Для кого"
                    onChange={({ selectedItem }) =>
                      setFilterVertical(selectedItem)
                    }
                  />
                </div>
              </div>
            ) : null}

            {isFilterType ? (
              <div className={s.block}>
                <div className={s.desktop}>
                  <Title level={5} weight="bold" className={s.subTitle}>
                    Тип материала
                  </Title>

                  <ListSelectItems data={TYPE_ARRAY} onChange={setFilterType} />
                </div>

                <div className={s.tablet}>
                  <Select
                    options={TYPE_ARRAY}
                    value={filterType}
                    label="Тип материала"
                    onChange={({ selectedItem }) => setFilterType(selectedItem)}
                  />
                </div>
              </div>
            ) : null}
          </div>
          <div className={s.cell}>
            {isLoading ? (
              <Loader />
            ) : (
              <>
                {events?.length > 0 ? (
                  eventsArray.map((event) => (
                    <EventCard
                      title={event?.fields?.title}
                      description={event?.fields?.description}
                      type={event?.fields?.type}
                      link={event?.fields?.link}
                      imageBgr={
                        event?.fields?.picture
                          ? event?.fields?.picture[0].url
                          : ''
                      }
                      verticalType={event?.fields?.verticalType}
                      vertical={event?.fields?.vertical}
                      key={event.id}
                    />
                  ))
                ) : (
                  <EventCard title="Курсы и материалы не найдены" disabled />
                )}

                {events?.length > 3 ? (
                  <Button
                    size="extra-large"
                    type="bordered"
                    fullWidth
                    onClick={handleToggleShowMore}
                    className={s.button}>
                    {isShowMore ? 'Свернуть' : 'Показать ещё'}
                  </Button>
                ) : null}
              </>
            )}
          </div>
        </div>
      </div>
    </section>
  );
}

CoursesMaterials.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  className: PropTypes.string,
  isFilterWho: PropTypes.bool,
  isFilterType: PropTypes.bool,
  onGetMaterials: PropTypes.func,
  isLoading: PropTypes.bool,
  events: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      description: PropTypes.string,
      link: PropTypes.string,
      categories: PropTypes.arrayOf,
    }),
  ),
};

CoursesMaterials.defaultProps = {
  title: 'Курсы и материалы',
  description:
    'Быстрые ответы на вопросы и практические советы из разных сфер бизнеса',
  className: null,
  isFilterWho: true,
  isFilterType: true,
  events: [],
  onGetMaterials: null,
  isLoading: false,
};

CoursesMaterials.displayName = 'CoursesMaterials';

export default CoursesMaterials;
