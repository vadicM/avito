import { useContext } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import PropTypes from 'prop-types';

import cn from 'classnames';

import Button from 'components/Button';
import Select from 'components/Select';
import useStickyHeader from 'hooks/useSticky';

import { SELECT_ANY } from 'constants/recommendations';

import { FilterContext } from 'context/FiltersContext';

import s from './SelectTraining.module.css';

import {
  EXPERIENCE_VALUES,
  BUSINESS_AREA_VALUES,
  WANT_LEARN_VALUES,
} from './constants';

function SelectTraining({
  className,
  classNameElem,
  onClick,
  defaultExperienceValue,
  defaultBusinessValue,
  defaultLearnValue,
  isMobileFooter,
  innerRef,
}) {
  const { elemRef, isSticky } = useStickyHeader();

  const navigate = useNavigate();

  const location = useLocation(false);

  const {
    valueExperience,
    setValueExperience,

    valueBusiness,
    setValueBusiness,

    valueLearn,
    setValueLearn,
  } = useContext(FilterContext);

  const handleClickToRecomendation = () => {
    if (location.pathname !== '/recommendations') navigate('/recommendations');
    if (onClick) {
      onClick();
    }
  };

  return (
    <section
      className={cn(s.root, isSticky && s.active, className)}
      ref={elemRef}>
      <div className="container">
        <div className={cn(s.block, classNameElem)}>
          <div className={s.cell}>
            <Select
              label="Опыт на Авито"
              value={valueExperience}
              onChange={(changes) => setValueExperience(changes.selectedItem)}
              options={EXPERIENCE_VALUES}
            />
          </div>
          <div className={s.cell}>
            <Select
              label="Сфера бизнеса"
              value={valueBusiness}
              onChange={(changes) => setValueBusiness(changes.selectedItem)}
              options={BUSINESS_AREA_VALUES}
            />
          </div>
          <div className={s.cell}>
            <Select
              label="Чему хотите научиться"
              value={valueLearn}
              onChange={(changes) => setValueLearn(changes.selectedItem)}
              options={WANT_LEARN_VALUES}
            />
          </div>
          <div className={cn(s.cell, s.cellLast)} ref={innerRef}>
            <Button
              size="large"
              className={s.button}
              onClick={handleClickToRecomendation}>
              Подобрать обучение
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
}

SelectTraining.propTypes = {
  className: PropTypes.string,
  classNameElem: PropTypes.string,
  onClick: PropTypes.func,
  isMobileFooter: PropTypes.bool,
  defaultExperienceValue: PropTypes.shape({
    value: PropTypes.string,
    title: PropTypes.string,
  }),
  defaultBusinessValue: PropTypes.shape({
    value: PropTypes.string,
    title: PropTypes.string,
  }),
  defaultLearnValue: PropTypes.shape({
    value: PropTypes.string,
    title: PropTypes.string,
  }),
  innerRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
};

SelectTraining.defaultProps = {
  className: null,
  classNameElem: null,
  onClick: null,
  isMobileFooter: false,
  defaultExperienceValue: {
    value: SELECT_ANY,
    title: 'Без опыта',
  },
  defaultBusinessValue: {
    value: SELECT_ANY,
    title: 'Любая',
  },
  defaultLearnValue: {
    value: SELECT_ANY,
    title: 'Всему',
  },
  innerRef: null,
};

SelectTraining.displayName = 'SelectTraining';

export default SelectTraining;
