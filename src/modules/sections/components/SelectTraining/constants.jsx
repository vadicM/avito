import {
  SELECT_AVITO_PPORTUNITIES,
  SELECT_ANY,
  SELECT_BEGINNER,
  SELECT_PRO,
  SELECT_BUILDSALES,
  SELECT_RUNBUSINESS,
  SELECT_ATTRACT_СUSTOMERS,
  SELECT_TRANSPORT,
  SELECT_HR,
  SELECT_REALTY,
  SELECT_SALES,
  SELECT_SERVICES,
} from 'constants/recommendations';

export const EXPERIENCE_VALUES = [
  {
    value: SELECT_ANY,
    title: 'Без опыта',
  },
  {
    value: SELECT_BEGINNER,
    title: 'Новичок',
  },
  {
    value: SELECT_PRO,
    title: 'Профессионал',
  },
];

export const WANT_LEARN_VALUES = [
  {
    value: SELECT_ANY,
    title: 'Всему',
  },
  {
    value: SELECT_AVITO_PPORTUNITIES,
    title: 'Использовать возможности Авито',
  },
  {
    value: SELECT_BUILDSALES,
    title: 'Выстраивать продажи',
  },
  {
    value: SELECT_RUNBUSINESS,
    title: 'Управлять бизнесом',
  },
  {
    value: SELECT_ATTRACT_СUSTOMERS,
    title: 'Привлекать клиентов',
  },
];

export const BUSINESS_AREA_VALUES = [
  {
    value: SELECT_ANY,
    title: 'Любая',
  },
  {
    value: SELECT_TRANSPORT,
    title: 'Транспорт',
  },
  {
    value: SELECT_HR,
    title: 'HR',
  },
  {
    value: SELECT_REALTY,
    title: 'Недвижимость',
  },
  {
    value: SELECT_SALES,
    title: 'Продажа товаров',
  },
  {
    value: SELECT_SERVICES,
    title: 'Оказание услуг',
  },
];
