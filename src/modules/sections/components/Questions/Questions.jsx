import PropTypes from 'prop-types';

import Title from 'components/Title';
import Text from 'components/Text';
import TabElement from 'components/TabElement';

import s from './Questions.module.css';
import { QUESTIONS_ARRAY } from './constants';

function Questions({ data }) {
  return (
    <section className={s.root}>
      <div className="container">
        <div className={s.row}>
          <aside className={s.aside}>
            <Title level={2} weight="bold" className={s.title}>
              Остались вопросы?
            </Title>
          </aside>
          <div className={s.cell}>
            {data?.length > 0
              ? data.map(({ id, fields }) => (
                  <TabElement title={fields?.title} key={id} bordered full>
                    {fields.content ? (
                      <Text className={s.content}>{fields.content}</Text>
                    ) : null}
                  </TabElement>
                ))
              : null}
          </div>
        </div>
      </div>
    </section>
  );
}

Questions.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      fields: PropTypes.objectOf,
    }),
  ),
};

Questions.defaultProps = {
  data: QUESTIONS_ARRAY,
};

Questions.displayName = 'Questions';

export default Questions;
