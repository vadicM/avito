/* eslint-disable import/prefer-default-export */
import pic1 from 'assets/images/learning/mouthpiece.svg';
import pic2 from 'assets/images/learning/time-chat.svg';
import pic3 from 'assets/images/learning/bubble.svg';
import pic4 from 'assets/images/learning/moneybox.svg';

export const LEARN_ARRAY = [
  {
    id: '1',
    text: 'Составлять вакансии так, чтобы приходило больше подходящих откликов',
    image: pic1,
  },
  {
    id: '2',
    text: 'Искать и фильтровать резюме, чтобы на подбор уходило меньше времени',
    image: pic2,
  },
  {
    id: '3',
    text: 'Использовать бесплатные возможности Авито, чтобы повысить доверие к компании',
    image: pic3,
  },
  {
    id: '4',
    text: 'Понимать, когда нужны платные услуги, а когда можно не тратить деньги',
    image: pic4,
  },
];
