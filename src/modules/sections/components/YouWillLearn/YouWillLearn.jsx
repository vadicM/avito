import PropTypes from 'prop-types';

import Title from 'components/Title';
import Text from 'components/Text';

import s from './YouWillLearn.module.css';
import { LEARN_ARRAY } from './constants';

function YouWillLearn({ data }) {
  return (
    <section className={s.root}>
      <div className="container">
        <Title level={2} weight="bold" className={s.title}>
          Вы научитесь
        </Title>
        <ul className={s.list}>
          {data.map(({ id, text, img }) => (
            <li key={id} className={s.listItem}>
              <img src={img} alt={text || 'image'} className={s.image} />
              <Text className={s.text} size="large">
                {text}
              </Text>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
}

YouWillLearn.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      text: PropTypes.string,
      img: PropTypes.string,
    }),
  ),
};

YouWillLearn.defaultProps = {
  data: LEARN_ARRAY,
};

YouWillLearn.displayName = 'YouWillLearn';

export default YouWillLearn;
