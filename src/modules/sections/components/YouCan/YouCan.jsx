import { useMemo, useState } from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import Title from 'components/Title';
import { YOU_CAN_ARRAY } from 'modules/sections/constants';
import Text from 'components/Text';

import Button from 'components/Button';
import useViewport from 'hooks/useViewport';

import s from './YouCan.module.css';

function YouCan({ data }) {
  const { isMobile } = useViewport();
  const [isTabActive, seIsTabActive] = useState(false);

  const handleOpenTab = () => {
    seIsTabActive(true);
  };

  const handleCloeTab = () => {
    seIsTabActive(false);
  };

  const filteredData = useMemo(() => {
    if (!isMobile) {
      return data;
    }
    if (isTabActive) {
      return data.filter((elem) => elem.category === 'Для новичков');
    }
    return data.filter((elem) => elem.category !== 'Для новичков');
  }, [data, isMobile, isTabActive]);

  if (data?.length === 0) {
    return null;
  }

  return (
    <section className={s.root}>
      <div className="container">
        <Title level={2} className={s.title} weight="bold">
          Вы узнаете
        </Title>

        {isMobile ? (
          <nav className={s.nav}>
            <Button
              type="ghost"
              className={cn(s.navItem, isTabActive && s.active)}
              onClick={handleOpenTab}>
              Для новичков
            </Button>

            <Button
              type="ghost"
              className={cn(s.navItem, !isTabActive && s.active)}
              onClick={handleCloeTab}>
              Для опытных
            </Button>
          </nav>
        ) : null}

        <div className={s.row}>
          {filteredData.map(({ id, category, description, img }) => (
            <div className={s.cell} key={id}>
              <img
                src={img}
                alt={description}
                width="72"
                height="72"
                className={s.image}
              />
              <div className={s.content}>
                <Text size="large" muted className={s.subTitle}>
                  {category}
                </Text>
                <Text size="large" className={s.text}>
                  {description}
                </Text>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}

YouCan.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      category: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      img: PropTypes.string.isRequired,
    }),
  ),
};

YouCan.defaultProps = {
  data: YOU_CAN_ARRAY,
};

YouCan.displayName = 'YouCan';

export default YouCan;
