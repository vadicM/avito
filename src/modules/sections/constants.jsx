import pic1 from 'assets/contentImages/you-can/pic1.png';
import pic2 from 'assets/contentImages/you-can/pic2.png';
import pic3 from 'assets/contentImages/you-can/pic3.png';
import pic4 from 'assets/contentImages/you-can/pic4.png';
import pic5 from 'assets/contentImages/you-can/pic5.png';
import pic6 from 'assets/contentImages/you-can/pic6.png';

import bgrImg1 from 'assets/images/background/program1.png';
import bgrImg2 from 'assets/images/background/program2.png';
import bgrImg3 from 'assets/images/background/program3.png';

export const YOU_CAN_ARRAY = [
  {
    id: '1',
    category: 'Для новичков',
    description:
      'Какие тонкости работы с Авито помогут сэкономить время и деньги на старте',
    img: pic1,
  },
  {
    id: '2',
    category: 'Для новичков',
    description: 'Как оформить профиль, чтобы выделиться среди конкурентов',
    img: pic2,
  },
  {
    id: '3',
    category: 'Для новичков',
    description:
      'Как получить первые заявки и стабильно увеличивать поток клиентов',
    img: pic3,
  },
  {
    id: '4',
    category: 'Для опытных',
    description:
      'Как эффективнее работать со стратегией продвижения и личным брендом',
    img: pic4,
  },
  {
    id: '5',
    category: 'Для опытных',
    description: 'Как расширить охват целевой аудитории',
    img: pic5,
  },
  {
    id: '6',
    category: 'Для опытных',
    description: 'Как увеличивать конверсию в сделку',
    img: pic6,
  },
];

export const SPECIALIZATIONS_ARRAY = [
  {
    id: '1',
    title: 'Продажи',
    description:
      'Научитесь правильно общаться и выстраивать долгосрочные отношения с клиентами, чтобы продавать больше и быстрее',
    peopleCount: 1000,
    imageBgr: bgrImg1,
    contentArray: [
      {
        id: '11',
        category: 'course',
        count: 2,
      },
      {
        id: '22',
        category: 'articles',
        count: 12,
      },
      {
        id: '33',
        category: 'video',
        count: 12,
      },
    ],
  },
  {
    id: '2',
    title: 'Маркетинг',
    description:
      'Поймёте, как привлекать клиентов, развивать личный бренд и как построить стратегию размещения',
    peopleCount: 35,
    imageBgr: bgrImg2,
    contentArray: [
      {
        id: '11',
        category: 'course',
        count: 7,
      },
      {
        id: '22',
        category: 'articles',
        count: 10,
      },
      {
        id: '33',
        category: 'video',
        count: 30,
      },
    ],
  },
  {
    id: '3',
    title: 'Менеджмент',
    description:
      'Прокачаете навыки и личные качества, которые помогут эффективно управлять командой и добиться успеха',
    peopleCount: 534,
    imageBgr: bgrImg3,
    contentArray: [
      {
        id: '11',
        category: 'course',
        count: 7,
      },
      {
        id: '22',
        category: 'articles',
        count: 4,
      },
      {
        id: '33',
        category: 'video',
        count: 4,
      },
    ],
  },
];
