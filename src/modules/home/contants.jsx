import {
  OkIcon,
  TelegramIcon,
  TwiiterIcon,
  VkIcon,
  YoutubeIcon,
} from 'components/Icons';

export const HEADER_LINKS = [
  {
    id: '0',
    title: 'Специализации',
    description: 'Курсы, статьи и вебинары, объединённые одной темой',
    link: '/specialization',
    subItems: [
      {
        id: '01',
        subTitle: 'Авито',
        link: '/specialization',
      },
      {
        id: '02',
        subTitle: 'Продажи',
        link: '/specialization',
      },
      {
        id: '03',
        subTitle: 'Менеджмент',
        link: '/specialization',
      },
    ],
  },
  {
    id: '1',
    title: 'Курсы и материалы',
    description:
      'Быстрые ответы на вопросы и практические советы из разных сфер бизнеса',
    link: '/school',
    subItems: [
      {
        id: '01',
        subTitle: 'Продавцам транспорта',
        link: '/school',
      },
      {
        id: '02',
        subTitle: 'Работодателям',
        link: '/school',
      },
      {
        id: '03',
        subTitle: 'Профессионалам недвижимости',
        link: '/school',
      },
      {
        id: '04',
        subTitle: 'Тем, кто продаёт товары',
        link: '/school',
      },
      {
        id: '05',
        subTitle: 'Тем, кто оказывает услуги',
        link: '/school',
      },
    ],
  },
  {
    id: '2',
    title: 'Моё обучение',
    link: '/',
  },
];

export const FOOTER_LINKS = [
  {
    id: '0',
    title: 'Специализации',
    links: [
      {
        id: '01',
        title: 'Авито',
        link: '/',
      },
      {
        id: '02',
        title: 'Продажи',
        link: '/',
      },
      {
        id: '03',
        title: 'Маркетинг',
        link: '/',
      },
      {
        id: '04',
        title: 'Бизнес',
        link: '/',
      },
    ],
  },
  {
    id: '1',
    title: 'Курсы и материалы',
    links: [
      {
        id: '10',
        title: 'Продавцам транспорта',
        link: '/',
      },
      {
        id: '11',
        title: 'Работодателям',
        link: '/',
      },
      {
        id: '12',
        title: 'Тем, кто продаёт товары',
        link: '/',
      },
      {
        id: '13',
        title: 'Профессионалам недвижимости',
        link: '/',
      },
      {
        id: '14',
        title: 'Тем, кто оказывает услуги',
        link: '/',
      },
    ],
  },
];

export const BODY_ELEMENT = document.body;

export const SOCIAL_LINKS = [
  {
    id: '0',
    title: 'vk',
    link: 'https://vk.com/',
    icon: <VkIcon />,
  },
  {
    id: '1',
    title: 'ok',
    link: 'https://ok.ru/',
    icon: <OkIcon />,
  },
  {
    id: '2',
    title: 'youtube',
    link: 'https://www.youtube.com/',
    icon: <YoutubeIcon />,
  },
  {
    id: '3',
    title: 'twitter',
    link: 'https://twitter.com/',
    icon: <TwiiterIcon />,
  },
  {
    id: '4',
    title: 'telegram',
    link: 'https://web.telegram.org/',
    icon: <TelegramIcon />,
  },
];
