import { useState, useEffect, useCallback } from 'react';
import { Link, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Logo from 'components/Logo';

import Button from 'components/Button';
import Text from 'components/Text';
import { HamburgerIcon, CloseIcon } from 'components/Icons';

import MobileMenu from '../MobileMenu';
import Dropdown from '../Dropdown';

import s from './Header.module.css';

const SCROLL_SHADOW_HEIGHT = 50;

function Header({ className, verticals, specialization }) {
  const [isMenuActive, setIsMenuActive] = useState(false);

  const [isShadow, setIsChadow] = useState(false);

  const location = useLocation();

  const handleToggleMenu = () => {
    setIsMenuActive((prevState) => !prevState);
  };

  const handleScroll = useCallback(() => {
    const scrollTopHeight = window.scrollY;

    if (scrollTopHeight > SCROLL_SHADOW_HEIGHT) {
      if (!isShadow) {
        setIsChadow(true);
      }
    } else if (isShadow) {
      setIsChadow(false);
    }
  }, [setIsChadow, isShadow]);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);

  useEffect(() => {
    setIsMenuActive(false);
  }, [location]);

  return (
    <>
      <header className={cn(s.root, isShadow && s.shadow, className)}>
        <div className="container">
          <div className={s.row}>
            <div className={s.cell}>
              <Logo className={s.logo} />

              <Text size="small" className={s.text}>
                Бесплатная бизнес-школа
              </Text>
            </div>

            <div className={s.cell}>
              <ul className={s.list}>
                <li className={s.listItem}>
                  <Link to="/">
                    <Text size="medium">Специализации</Text>
                  </Link>
                  {specialization?.length > 0 ? (
                    <div className={s.dropdown}>
                      <Dropdown
                        links={specialization}
                        isSpecialization
                        description="Курсы, статьи и вебинары, объединённые одной темой"
                      />
                    </div>
                  ) : null}
                </li>
                <li className={s.listItem}>
                  <Link to="/">
                    <Text size="medium">Курсы и материалы</Text>
                  </Link>
                  {verticals?.length > 0 ? (
                    <div className={s.dropdown}>
                      <Dropdown
                        links={verticals}
                        isVertical
                        description="Быстрые ответы на вопросы и практические советы из разных сфер бизнеса"
                      />
                    </div>
                  ) : null}
                </li>
              </ul>
              <Button
                type="ghost"
                size="small"
                onClick={handleToggleMenu}
                className={s.button}>
                {isMenuActive ? <CloseIcon /> : <HamburgerIcon />}
              </Button>
            </div>
          </div>
        </div>
      </header>
      {isMenuActive ? (
        <MobileMenu verticals={verticals} specialization={specialization} />
      ) : null}
    </>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  verticals: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.objectOf,
    }),
  ),
  specialization: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.objectOf,
    }),
  ),
};

Header.defaultProps = {
  className: null,
  verticals: null,
  specialization: null,
};

Header.displayName = 'Header';

export default Header;
