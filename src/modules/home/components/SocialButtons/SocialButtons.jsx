import { SOCIAL_LINKS } from 'modules/home/contants';

import s from './SocialButtons.module.css';

function SocialButtons() {
  return (
    <ul className={s.root}>
      {SOCIAL_LINKS.map(({ id, title, link, icon }) => (
        <li key={id} className={s.item}>
          <a
            href={link}
            rel="noopener noreferrer"
            target="_blank"
            title={title}>
            {icon}
          </a>
        </li>
      ))}
    </ul>
  );
}

export default SocialButtons;
