import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Text from 'components/Text';
import Title from 'components/Title';
import impPeople1 from 'assets/images/people/pic1.jpg';
import impPeople2 from 'assets/images/people/pic2.jpg';
import impPeople3 from 'assets/images/people/pic3.jpg';
import { numberWithSpaces } from 'utils/numberWithSpaces';
import { getNoun } from 'utils/getNoun';

import Button from 'components/Button';
import { ArrowRightIcon } from 'components/Icons';

import bgrImg1 from 'assets/images/background/program1.png';
import bgrImg2 from 'assets/images/background/program2.png';
import bgrImg3 from 'assets/images/background/program3.png';

import s from './SpecializationCard.module.css';

function SpecializationCard({
  className,
  id,
  title,
  description,
  link,
  peopleCount,
  contentArray,
  index,
  imageBgr,
  wide,
  courseCount,
  articleCount,
  videoCount,
}) {
  const formatedPeopleCount = numberWithSpaces(peopleCount);

  const correctImageBgr = () => {
    let img = '';
    if (index % 1 === 0) {
      img = bgrImg1;
    }
    if (index % 2 === 0) {
      img = bgrImg2;
    }
    if (index % 3 === 0) {
      img = bgrImg3;
    }
    return img;
  };

  const backgroundImg = correctImageBgr();

  return (
    <Link className={cn(s.root, wide && s.wide, className)} to={link}>
      {backgroundImg ? (
        <img src={backgroundImg} alt="bgr" className={s.background} />
      ) : null}

      <div className={s.content}>
        <Title level={3} className={s.title} weight="bold">
          {title}
        </Title>

        <Text size="medium" className={s.description}>
          {description}
        </Text>

        <div className={s.countWrap}>
          <ul className={s.imgList}>
            <li className={s.imgItem}>
              <img src={impPeople1} alt="people" />
            </li>

            <li className={s.imgItem}>
              <img src={impPeople2} alt="people" />
            </li>

            <li className={s.imgItem}>
              <img src={impPeople3} alt="people" />
            </li>
          </ul>

          <span className={s.counItem}>+{formatedPeopleCount}</span>

          <Text size="small" muted>
            сейчас <br />
            учатся здесь
          </Text>
        </div>

        <footer className={s.footer}>
          <ul className={s.contentList}>
            {courseCount ? (
              <li className={s.contentItem}>
                <Title level={4} weight="bold" className={s.contentTitle}>
                  {courseCount}
                </Title>

                <Text className={s.contentDescriotion} muted>
                  {getNoun(courseCount, 'курс', 'курса', 'курсов')}
                </Text>
              </li>
            ) : null}
            {articleCount ? (
              <li className={s.contentItem}>
                <Title level={4} weight="bold" className={s.contentTitle}>
                  {articleCount}
                </Title>

                <Text className={s.contentDescriotion} muted>
                  {getNoun(articleCount, 'статья', 'статьи', 'статей')}
                </Text>
              </li>
            ) : null}
            {videoCount ? (
              <li className={s.contentItem}>
                <Title level={4} weight="bold" className={s.contentTitle}>
                  {videoCount}
                </Title>

                <Text className={s.contentDescriotion} muted>
                  {getNoun(videoCount, 'статья', 'статьи', 'статей')}
                </Text>
              </li>
            ) : null}
          </ul>
          {/* {contentArray?.length > 0 ? (
            <ul className={s.contentList}>
              {contentArray.map((item, indexCont) => {
                let currentTitle = null;

                const getTitle = () => {
                  if (item.category === 'course') {
                    currentTitle = ['курс', 'курса', 'курсов'];
                  }
                  if (item.category === 'articles') {
                    currentTitle = ['статья', 'статьи', 'статей'];
                  }
                  if (item.category === 'video') {
                    currentTitle = ['видео', 'видео', 'видео'];
                  }
                };

                getTitle();
                const formatedTitle = currentTitle
                  ? getNoun(
                      item.count,
                      currentTitle[0],
                      currentTitle[1],
                      currentTitle[2],
                    )
                  : '';

                if (!formatedTitle) {
                  return null;
                }

                return (
                  <li key={item.id} className={s.contentItem}>
                    <Title level={4} weight="bold" className={s.contentTitle}>
                      {item.count}
                    </Title>

                    <Text className={s.contentDescriotion} muted>
                      {formatedTitle}
                    </Text>
                  </li>
                );
              })}
            </ul>
          ) : null} */}

          <Button type="ghost" className={s.button}>
            <ArrowRightIcon />
          </Button>
        </footer>
      </div>
    </Link>
  );
}

SpecializationCard.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  wide: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.string,
  link: PropTypes.string,
  imageBgr: PropTypes.string,
  peopleCount: PropTypes.number,
  index: PropTypes.number,
  courseCount: PropTypes.number,
  articleCount: PropTypes.number,
  videoCount: PropTypes.number,
  contentArray: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      category: PropTypes.string,
      count: PropTypes.number,
    }),
  ),
};

SpecializationCard.defaultProps = {
  className: null,
  wide: false,
  id: '',
  title: '',
  description: '',
  link: '#',
  imageBgr: '',
  peopleCount: 0,
  index: 0,
  contentArray: [],
  courseCount: null,
  articleCount: null,
  videoCount: null,
};

SpecializationCard.displayName = 'SpecializationsCard';

export default SpecializationCard;
