import { useState, useCallback } from 'react';

import Button from 'components/Button';

import { ArrowRightIcon } from 'components/Icons';

import TextArea from 'components/TextArea/TextArea';

import { Suggestions } from 'config/api';

import Loader from 'components/Loader';
import ModalSuccess from '../ModalSuccess';

import s from './SuggestionsForm.module.css';

function SuggestionsForm() {
  const [value, setValue] = useState('');

  const [openModal, setOpenModal] = useState(false);

  const [loading, setLoading] = useState(false);

  const handleSubmitForm = (event) => {
    event.preventDefault();

    if (value) {
      onSendSuggestion(value);
    }
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const onSendSuggestion = useCallback(
    async (text) => {
      setLoading(true);
      await Suggestions.postSuggestions(text)
        .then((resData) => {
          setLoading(false);
          setValue('');
          setOpenModal(true);
        })

        .catch((errorCode) => {
          // eslint-disable-next-line no-console
          console.log('error', errorCode);
        })

        .finally(() => {
          setLoading(false);
        });
    },
    [setLoading],
  );

  return (
    <>
      <ModalSuccess open={openModal} onClose={handleCloseModal} />
      <div className={s.root}>
        <form onSubmit={handleSubmitForm}>
          <TextArea
            placeholder="Хочу узнать, как..."
            rows="1"
            value={value}
            name="textValue"
            required
            onChange={(event) => setValue(event.target.value)}
          />
          <Button
            type="ghost"
            htmlType="submit"
            disabled={loading}
            className={s.submitButton}>
            {loading ? <Loader isSmall /> : <ArrowRightIcon />}
          </Button>
        </form>
      </div>
    </>
  );
}

export default SuggestionsForm;
