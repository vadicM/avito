import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Logo from 'components/Logo';

import Text from 'components/Text';
import Title from 'components/Title';

import SocialButtons from '../SocialButtons';
import SuggestionsForm from '../SuggestionsForm';

import s from './Footer.module.css';

function Footer({ className, verticals, specialization }) {
  return (
    <footer className={cn(s.root, className)}>
      <div className="container">
        <div className={s.row}>
          <div className={s.cell}>
            <Logo size="large" className={s.logo} />
          </div>

          <div className={s.cell}>
            <div className={s.block}>
              <Title level={4} weight="bold" className={s.title}>
                Специализации
              </Title>

              <ul className={s.list}>
                {specialization.map((item) => (
                  <li className={s.listItem} key={item.id}>
                    {item.fields.link ? (
                      <Link to={`/specialization/${item.fields.link}`}>
                        <Text muted inline>
                          {item.fields.title}
                        </Text>
                      </Link>
                    ) : null}
                  </li>
                ))}
              </ul>
            </div>
          </div>

          <div className={cn(s.cell, s.cellMiddle)}>
            <Title level={4} weight="bold" className={cn(s.title, s.titleSub)}>
              Предложить тему
            </Title>

            <Text muted>
              Сделаем курс, если несколько человек заинтересуются предложенной
              вами темой
            </Text>

            <SuggestionsForm />
          </div>

          <div className={cn(s.cell, s.cellEnd)}>
            <Text muted>
              © Бизнес-школа Авито
              <br />
              ООО «КЕХ еКоммерц» 2007–2022.
            </Text>

            <br />
            <br />

            <Text muted>Сделано в студии «Луч»</Text>
          </div>

          <div className={s.cell}>
            <div className={s.block}>
              <Title level={4} weight="bold" className={s.title}>
                Курсы и материалы
              </Title>

              <ul className={s.list}>
                {verticals.map((item) => (
                  <li className={s.listItem} key={item.id}>
                    {item.fields.link ? (
                      <Link to={`/school/${item.fields.link}`}>
                        <Text muted inline>
                          {item.fields.title}
                        </Text>
                      </Link>
                    ) : null}
                  </li>
                ))}
              </ul>
            </div>
          </div>

          <div className={cn(s.cell, s.cellSocials)}>
            <Title level={4} weight="bold" className={cn(s.title, s.titleSub)}>
              Авито в соцсетях
            </Title>

            <SocialButtons />
          </div>
        </div>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  className: PropTypes.string,
  verticals: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.objectOf,
    }),
  ),
  specialization: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.objectOf,
    }),
  ),
};

Footer.defaultProps = {
  className: null,
  verticals: null,
  specialization: null,
};

Footer.displayName = 'Footer';

export default Footer;
