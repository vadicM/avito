/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from 'prop-types';
import Modal from 'components/Modal';

import Title from 'components/Title';
import successImg from 'assets/images/modal/success.png';

import s from './ModalSuccess.module.css';

function ModalSuccess({ className, open, ...props }) {
  return (
    <Modal open={open} position="right center" {...props}>
      <div className={s.root}>
        <img
          src={successImg}
          alt="successImg"
          className={s.image}
          width="122"
          height="80"
        />
        <Title level={4} className={s.title} weight="bold">
          Вы предложили тему
        </Title>
      </div>
    </Modal>
  );
}

ModalSuccess.propTypes = {
  className: PropTypes.string,
  open: PropTypes.bool,
};

ModalSuccess.defaultProps = {
  className: null,
  open: false,
};

ModalSuccess.displayName = 'ModalSuccess';

export default ModalSuccess;
