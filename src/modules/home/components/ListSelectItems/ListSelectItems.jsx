import { useState } from 'react';
import cn from 'classnames';
import Button from 'components/Button';
import Text from 'components/Text';
import PropTypes from 'prop-types';

import s from './ListSelectItems.module.css';

function ListSelectItems({ className, data, onChange }) {
  const [activeIndex, setActiveIndex] = useState(0);

  if (data?.length === 0) {
    return null;
  }

  const handleClickButton = (indexParam, value) => {
    setActiveIndex(indexParam);
    onChange(value);
  };

  return (
    <ul className={cn(s.root, className)}>
      {data.map((item, index) => (
        <li key={item.id} className={s.rootItem}>
          <Button
            type="ghost"
            className={cn(s.link, activeIndex === index && s.active)}
            onClick={() => handleClickButton(index, item)}>
            <Text inline>{item.title}</Text>
          </Button>
        </li>
      ))}
    </ul>
  );
}

ListSelectItems.propTypes = {
  className: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
    }),
  ),
  onChange: PropTypes.func,
};

ListSelectItems.defaultProps = {
  className: null,
  data: [],
  onChange: () => null,
};

ListSelectItems.displayName = 'ListSelectItems';

export default ListSelectItems;
