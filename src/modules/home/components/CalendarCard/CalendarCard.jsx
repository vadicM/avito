import cn from 'classnames';

import Text from 'components/Text';
import PropTypes from 'prop-types';

import s from './CalendarCard.module.css';

function CalendarCard({
  className,
  dayNumber,
  day,
  active,
  disabled,
  onClick,
}) {
  const rootClassName = cn(s.root, 'calendar-card', {
    [s.active]: active,
    [s.disabled]: disabled,
  });
  return (
    <button className={rootClassName} onClick={onClick} type="button">
      <Text className={s.subText}>{day}</Text>
      <Text weight="bold" className={s.text}>
        {dayNumber}
      </Text>
    </button>
  );
}

CalendarCard.propTypes = {
  className: PropTypes.string,
  dayNumber: PropTypes.number,
  day: PropTypes.string,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

CalendarCard.defaultProps = {
  className: null,
  dayNumber: 0,
  day: '',
  active: false,
  disabled: false,
  onClick: () => null,
};

CalendarCard.displayName = 'CalendarCard';

export default CalendarCard;
