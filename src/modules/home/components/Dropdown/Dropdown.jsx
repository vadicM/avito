import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

import cn from 'classnames';

import Text from 'components/Text';

import s from './Dropdown.module.css';

function Dropdown({
  className,
  description,
  links,
  isSpecialization,
  isVertical,
}) {
  return (
    <div className={cn(s.root, className)}>
      <ul className={s.list}>
        {links.map(({ id, fields }) => (
          <li className={s.listItem} key={id || fields.title}>
            {fields.link ? (
              <Link
                to={
                  isVertical
                    ? `/school/${fields.link}`
                    : `/specialization/${fields.link}`
                }>
                <Text className={s.text} size="large" weight="bold" inline>
                  {fields.title}
                </Text>
              </Link>
            ) : null}
          </li>
        ))}

        {description ? (
          <Text className={s.description} muted>
            {description}
          </Text>
        ) : null}
      </ul>
    </div>
  );
}

Dropdown.propTypes = {
  className: PropTypes.string,
  description: PropTypes.string,
  isSpecialization: PropTypes.bool,
  isVertical: PropTypes.bool,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      link: PropTypes.string,
    }),
  ),
};

Dropdown.defaultProps = {
  className: null,
  description: '',
  isSpecialization: false,
  isVertical: false,
  links: [],
};

Dropdown.displayName = 'Dropdown';

export default Dropdown;
