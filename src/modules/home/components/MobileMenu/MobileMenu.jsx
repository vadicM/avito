import { Link } from 'react-router-dom';
import ScrollLock, { TouchScrollable } from 'react-scrolllock';
import PropTypes from 'prop-types';

import Button from 'components/Button';
import Text from 'components/Text';
import Title from 'components/Title';

import s from './MobileMenu.module.css';

function MobileMenu({ verticals, specialization }) {
  return (
    <>
      <ScrollLock />
      <TouchScrollable>
        <div className={s.root}>
          <div className="container">
            <div className={s.block}>
              <Link to="/">
                <Title level={4} weight="bold" className={s.title}>
                  Специализации
                </Title>
              </Link>
              <ul className={s.list}>
                {specialization.length > 0
                  ? specialization.map((subItem) => (
                      <li className={s.listItem} key={subItem.id}>
                        <Link to={`/specialization/${subItem.fields.link}`}>
                          <Text>{subItem.fields.title}</Text>
                        </Link>
                      </li>
                    ))
                  : null}
              </ul>
            </div>
            <div className={s.block}>
              <Link to="/">
                <Title level={4} weight="bold" className={s.title}>
                  Курсы и материалы
                </Title>
              </Link>
              <ul className={s.list}>
                {verticals.length > 0
                  ? verticals.map((subItem) => (
                      <li className={s.listItem} key={subItem.id}>
                        <Link to={`/school/${subItem.fields.link}`}>
                          <Text>{subItem.fields.title}</Text>
                        </Link>
                      </li>
                    ))
                  : null}
              </ul>
            </div>
            <div className={s.block}>
              <Link to="/">
                <Title level={4} weight="bold" className={s.title}>
                  Моё обучение
                </Title>
              </Link>
            </div>

            <footer className={s.footer}>
              <Link to="/recommendations">
                <span>
                  <Button>Подобрать обучение</Button>
                </span>
              </Link>
            </footer>
          </div>
        </div>
      </TouchScrollable>
    </>
  );
}

MobileMenu.propTypes = {
  verticals: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.objectOf,
    }),
  ),
  specialization: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.objectOf,
    }),
  ),
};

MobileMenu.defaultProps = {
  verticals: null,
  specialization: null,
};

MobileMenu.displayName = 'MobileMenu';

export default MobileMenu;
