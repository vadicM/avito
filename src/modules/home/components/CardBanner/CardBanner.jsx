import PropTypes from 'prop-types';

import cn from 'classnames';

import Title from 'components/Title';
import Text from 'components/Text';
import Button from 'components/Button';

import impPeople1 from 'assets/images/people/pic1.jpg';
import impPeople2 from 'assets/images/people/pic2.jpg';
import impPeople3 from 'assets/images/people/pic3.jpg';

import { numberWithSpaces } from 'utils/numberWithSpaces';
import { getNoun } from 'utils/getNoun';

import useStickyHeader from 'hooks/useSticky';

import s from './CardBanner.module.css';

function CardBanner({
  className,
  backgroundImg,
  type,
  title,
  description,
  contentArray,
  categories,
  peopleCount,
}) {
  const { elemRef, isSticky } = useStickyHeader();

  const isSpecializationType = type === 'specialization';

  const formatedPeopleCount = numberWithSpaces(peopleCount);

  return (
    <>
      <div className={cn(s.fixedContent, isSticky && s.active)}>
        <div className={s.container}>
          <div className={s.categories}>
            {categories?.length > 0 ? (
              <ul className={s.list}>
                {categories.map((item) => (
                  <li key={item.id} className={s.listItem}>
                    {item.img ? (
                      <img
                        src={item.img}
                        alt={item.title}
                        className={s.image}
                        width="20"
                        height="20"
                      />
                    ) : null}
                    <Text className={s.text}>{item.title}</Text>
                  </li>
                ))}
              </ul>
            ) : null}
          </div>
          <Button className={s.button} size="large">
            Начать обучение
          </Button>
        </div>
      </div>

      <div className={cn(s.root, className)}>
        {backgroundImg ? (
          <img src={backgroundImg} alt="background" className={s.background} />
        ) : null}
        <div className={s.content}>
          {categories?.length > 0 ? (
            <ul className={s.list}>
              {categories.map((item) => (
                <li key={item.id} className={s.listItem}>
                  {item.img ? (
                    <img
                      src={item.img}
                      alt={item.title}
                      className={s.image}
                      width="20"
                      height="20"
                    />
                  ) : null}
                  <Text className={s.text}>{item.title}</Text>
                </li>
              ))}
            </ul>
          ) : null}

          <Title level={1} className={s.title} weight="bold">
            {title}
          </Title>

          <Text className={s.description} size="large">
            {description}
          </Text>

          {isSpecializationType && peopleCount ? (
            <div className={s.countWrap}>
              <ul className={s.imgList}>
                <li className={s.imgItem}>
                  <img src={impPeople1} alt="people" />
                </li>

                <li className={s.imgItem}>
                  <img src={impPeople2} alt="people" />
                </li>

                <li className={s.imgItem}>
                  <img src={impPeople3} alt="people" />
                </li>
              </ul>

              <span className={s.counItem}>+{formatedPeopleCount}</span>

              <Text size="medium" className={s.textItem} muted>
                сейчас
                <br />
                учатся здесь
              </Text>
            </div>
          ) : null}

          <div ref={elemRef} className={s.buttonWrapper}>
            <div className={s.buttonContainer}>
              <div className={s.container}>
                <div className={s.categories}>
                  {categories?.length > 0 ? (
                    <ul className={s.list}>
                      {categories.map((item) => (
                        <li key={item.id} className={s.listItem}>
                          {item.img ? (
                            <img
                              src={item.img}
                              alt={item.title}
                              className={s.image}
                              width="20"
                              height="20"
                            />
                          ) : null}
                          <Text className={s.text}>{item.title}</Text>
                        </li>
                      ))}
                    </ul>
                  ) : null}
                </div>
                <Button className={s.button} size="large">
                  Начать обучение
                </Button>
              </div>
            </div>
          </div>
        </div>

        <footer className={s.footer}>
          {contentArray?.length > 0 ? (
            <ul className={s.contentList}>
              {contentArray.map((item, indexCont) => {
                let currentTitle = null;

                const getTitle = () => {
                  if (item.category === 'course') {
                    currentTitle = ['курс', 'курса', 'курсов'];
                  }
                  if (item.category === 'articles') {
                    currentTitle = ['статья', 'статьи', 'статей'];
                  }
                  if (item.category === 'video') {
                    currentTitle = ['видео', 'видео', 'видео'];
                  }
                  if (item.category === 'exercise') {
                    currentTitle = ['задание', 'задания', 'заданий'];
                  }
                  if (item.category === 'topic') {
                    currentTitle = ['тема', 'темы', 'тем'];
                  }
                  if (item.category === 'materials') {
                    currentTitle = ['материал', 'материала', 'материалов'];
                  }
                };

                getTitle();
                const formatedTitle = currentTitle
                  ? getNoun(
                      item.count,
                      currentTitle[0],
                      currentTitle[1],
                      currentTitle[2],
                    )
                  : '';

                if (!formatedTitle) {
                  return null;
                }

                return (
                  <li key={item.id} className={s.contentItem}>
                    <Title level={4} weight="bold" className={s.contentTitle}>
                      {item.count}
                    </Title>

                    <Text className={s.contentDescriotion} muted>
                      {formatedTitle}
                    </Text>
                  </li>
                );
              })}
            </ul>
          ) : null}
        </footer>
      </div>
    </>
  );
}

CardBanner.propTypes = {
  className: PropTypes.string,
  backgroundImg: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  type: PropTypes.oneOf(['specialization', 'course']),
  peopleCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  contentArray: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      category: PropTypes.string,
      count: PropTypes.number,
    }),
  ),
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      img: PropTypes.string,
    }),
  ),
};

CardBanner.defaultProps = {
  className: null,
  backgroundImg: '',
  title: '',
  description: '',
  type: 'specialization',
  contentArray: [],
  categories: [],
  peopleCount: 0,
};

CardBanner.displayName = 'CardBanner';

export default CardBanner;
