import { isValidElement, useMemo } from 'react';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Title from 'components/Title';
import Text from 'components/Text';

import { ReactComponent as BookmarkImage } from 'assets/icons/categories/bookmark.svg';

import notesImage from 'assets/icons/categories/notes.svg';
import videoImage from 'assets/icons/categories/video.svg';
import articleImage from 'assets/icons/categories/article.svg';
import calendarImage from 'assets/icons/categories/calendar.svg';

import s from './EventCard.module.css';

function EventCard({
  className,
  title,
  link,
  description,
  imageBgr,
  categories,
  disabled,
  type,
  isDate,
  date,
  verticalType,
  vertical,
}) {
  const dataForType = useMemo(() => {
    let data = null;

    if (isDate) {
      data = {
        title: date,
        img: calendarImage,
      };
      return data;
    }

    if (type === 'course') {
      data = {
        title: 'Курсы',
        img: notesImage,
      };
    }

    if (type === 'video') {
      data = {
        title: 'Видео',
        img: videoImage,
      };
    }

    if (type === 'article') {
      data = {
        title: 'Статьи',
        img: articleImage,
      };
    }
    return data;
  }, [type, isDate, date]);

  return (
    <article className={cn(s.root, disabled && s.disabled, className)}>
      <a
        href={link}
        className={s.globalLink}
        rel="noopener noreferrer"
        target="_blank">
        {title}
      </a>
      {imageBgr ? (
        <img
          src={imageBgr}
          alt={title || 'background'}
          className={s.background}
        />
      ) : null}

      <div className={cn(s.content, imageBgr && s.hasBackground)}>
        <Title level={5} weight="bold" className={s.title}>
          {title}
        </Title>

        {description ? (
          <Text className={s.description}>{description}</Text>
        ) : null}

        <ul className={s.list}>
          {dataForType ? (
            <li className={s.listItem}>
              <img
                src={dataForType.img}
                alt="notesImage"
                className={s.image}
                width="20"
                height="20"
              />
              <Text className={s.text}>{dataForType.title}</Text>
            </li>
          ) : null}

          {verticalType ? (
            <li className={s.listItem}>
              <Link to={`/vertical/${vertical}`} className={s.listLink}>
                <span className={s.image}>
                  <BookmarkImage />
                </span>

                <Text className={s.text}>{verticalType}</Text>
              </Link>
            </li>
          ) : null}
        </ul>

        {categories?.length > 0 ? (
          <ul className={s.list}>
            {categories.map((item) => (
              <li className={s.listItem} key={item.id}>
                {item.link ? (
                  <Link to={item.link} className={s.listLink}>
                    {item.img && isValidElement(item.img) ? (
                      <span className={s.image}>{item.img}</span>
                    ) : null}

                    {item.img && typeof item.img === 'string' ? (
                      <img
                        src={item.img}
                        alt={item.title}
                        className={s.image}
                        width="20"
                        height="20"
                      />
                    ) : null}

                    <Text className={s.text}>{item.title}</Text>
                  </Link>
                ) : (
                  <>
                    {item.img && isValidElement(item.img) ? (
                      <span className={s.image}>{item.img}</span>
                    ) : null}

                    {item.img && typeof item.img === 'string' ? (
                      <img
                        src={item.img}
                        alt={item.title}
                        className={s.image}
                        width="20"
                        height="20"
                      />
                    ) : null}

                    <Text className={s.text}>{item.title}</Text>
                  </>
                )}
              </li>
            ))}
          </ul>
        ) : null}
      </div>
    </article>
  );
}

EventCard.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  imageBgr: PropTypes.string,
  link: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  isDate: PropTypes.bool,
  date: PropTypes.string,
  verticalType: PropTypes.string,
  vertical: PropTypes.string,
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      img: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    }),
  ),
};

EventCard.defaultProps = {
  className: null,
  title: '',
  description: '',
  imageBgr: '',
  link: '#',
  categories: [],
  disabled: false,
  type: '',
  isDate: false,
  date: '',
  verticalType: '',
  vertical: '',
};

export default EventCard;
