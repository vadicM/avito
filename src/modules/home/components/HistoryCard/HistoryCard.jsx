import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Text from 'components/Text';

import { PlayIcon } from 'components/Icons';
import s from './HistoryCard.module.css';

const getClassNameForBackground = (indexParam) => {
  if (indexParam % 2 === 0) {
    return s.index2;
  }
  if (indexParam % 3 === 0) {
    return s.index3;
  }
  return s.index1;
};

function HistoryCard({
  className,
  title,
  bigTitle,
  type,
  backgroundImg,
  author,
  link,
  index,
}) {
  const isVideo = type === 'video';

  const classNameBackground = getClassNameForBackground(index + 1);

  return (
    <article
      className={cn(
        s.root,
        isVideo && s.dark,
        !backgroundImg && classNameBackground,
        className,
      )}>
      {isVideo ? (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={link}
          className={s.link}>
          {link}
        </a>
      ) : (
        <Link to={link} className={s.link}>
          {link}
        </Link>
      )}

      {backgroundImg ? (
        <img src={backgroundImg} alt="img" className={s.backround} />
      ) : null}

      <div className={s.content}>
        {title ? (
          <Text className={s.title} weight="bold" size="large">
            {title}
          </Text>
        ) : null}

        {bigTitle ? (
          <Text className={s.bitTitle} weight="bold">
            {bigTitle}
          </Text>
        ) : null}
      </div>

      {author ? (
        <footer className={s.footer}>
          {isVideo ? (
            <span className={s.playIcon}>
              <PlayIcon />
            </span>
          ) : null}
          {!isVideo && author.img ? (
            <img
              src={author.img}
              alt={author.name || 'author'}
              className={s.footerImg}
              width="48"
              height="48"
            />
          ) : null}

          <div className={s.footerWrap}>
            {author.name ? <Text className={s.text}>{author.name}</Text> : null}

            {author.category ? <Text muted>{author.category}</Text> : null}
          </div>
        </footer>
      ) : null}
    </article>
  );
}

HistoryCard.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  bigTitle: PropTypes.string,
  type: PropTypes.string,
  backgroundImg: PropTypes.string,
  link: PropTypes.string,
  index: PropTypes.number,
  author: PropTypes.shape({
    img: PropTypes.string,
    name: PropTypes.string,
    category: PropTypes.string,
  }),
};

HistoryCard.defaultProps = {
  className: null,
  title: '',
  bigTitle: '',
  type: 'string',
  backgroundImg: null,
  author: null,
  link: '#',
  index: null,
};

HistoryCard.displayName = 'HistoryCard';

export default HistoryCard;
