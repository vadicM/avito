import Footer from 'modules/home/components/Footer';
import Header from 'modules/home/components/Header';

import useSpecialization from 'modules/sections/hooks/useSpecialization';
import useVerticals from 'modules/sections/hooks/useVerticals';
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import s from './CommonLayout.module.css';

interface Props {
  children: JSX.Element;
}

export default function CommonLayout({ children }: Props) {
  const { verticals } = useVerticals();
  const { specializations } = useSpecialization();

  const location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  return (
    <>
      <Header specialization={specializations} verticals={verticals} />

      <main className={s.root}>{children}</main>

      <Footer
        className={s.footer}
        specialization={specializations}
        verticals={verticals}
      />
    </>
  );
}
