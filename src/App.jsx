import { useEffect } from 'react';
import { Routes, Route, BrowserRouter } from 'react-router-dom';

import Vertical from 'pages/Vertical';
import { FilterProvider } from 'context/FiltersContext';
import Home from './pages/Home';
import Specialization from './pages/Specialization';
import Recommendations from './pages/Recommendations';
import Page404 from './pages/404';
import './App.css';

const appHeight = () => {
  const doc = document.documentElement;
  doc.style.setProperty('--app-height', `${window.innerHeight}px`);
};

function App() {
  useEffect(() => {
    appHeight();
  }, []);

  useEffect(() => {
    window.addEventListener('resize', appHeight);

    return () => {
      window.removeEventListener('resize', appHeight);
    };
  }, []);

  return (
    <FilterProvider>
      <BrowserRouter basename="/avito">
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route
            path="/specialization/:specializationId"
            exact
            element={<Specialization />}
          />
          <Route path="/recommendations" exact element={<Recommendations />} />
          <Route path="/school/:verticalId" exact element={<Vertical />} />
          <Route path="*" element={<Page404 />} />
        </Routes>
      </BrowserRouter>
    </FilterProvider>
  );
}

export default App;
