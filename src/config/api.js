import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://api.airtable.com/v0/apptfz5AQArbTkLRz',
  withCredentials: false,
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
  },
});

const responseBody = (response) => response.data;

const requests = {
  get: (url) => instance.get(url).then(responseBody),
  post: (url, body) => instance.post(url, body).then(responseBody),
  put: (url, body) => instance.put(url, body).then(responseBody),
  delete: (url) => instance.delete(url).then(responseBody),
};

export const MainPage = {
  getBannerTable: () => requests.get('/main'),
};

export const Specializations = {
  getSpecializations: () => requests.get('/specialization'),
};

const getParams = (type, vertical) => {
  if (type && vertical) {
    return `AND(type = '${type}', vertical = '${vertical}')`;
  }
  if (type && !vertical) {
    return `type='${type}'`;
  }
  if (!type && vertical) {
    return `vertical='${vertical}'`;
  }

  return null;
};

export const Materials = {
  getMaterials: (type, vertical) =>
    instance
      .get('/materials', {
        params: {
          filterByFormula: getParams(type, vertical),
        },
      })
      .then(responseBody),
};

export const Events = {
  getEvents: (dateMinusDay, datePlusDay) =>
    instance
      .get('/events', {
        params: {
          filterByFormula:
            dateMinusDay && datePlusDay
              ? `AND(IS_AFTER((dateStart), "${dateMinusDay}"), IS_BEFORE((dateStart), "${datePlusDay}"))`
              : null,
        },
      })
      .then(responseBody),
};

export const Stories = {
  getStories: () => requests.get('/stories'),
};

export const Recomendation = {
  getCourses: () =>
    instance
      .get('/materials', {
        params: {
          filterByFormula: `type='course'`,
        },
      })
      .then(responseBody),
};

export const Verticals = {
  getVerticals: () => requests.get('/verticals'),
};

export const Questions = {
  getQuestions: () => requests.get('/questions'),
};

export const Modules = {
  getModules: (type) => requests.get(`/module_${type}`),
};

export const Suggestions = {
  postSuggestions: (text) =>
    requests.post('/suggestions', {
      fields: {
        text,
      },
    }),
};
