/* eslint-disable import/prefer-default-export */
const days = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];

export const getDayName = (date) => days[date.getDay()];

export const getDayNumber = (date) => date.getDate();
