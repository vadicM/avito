// eslint-disable-next-line import/prefer-default-export
export function numberWithSpaces(value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}
