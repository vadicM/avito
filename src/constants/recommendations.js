export const EXPERIENCE = 'experience';
export const BUSINESS = 'business';
export const LEARNING = 'learning';

export const SELECT_ANY = 'any';

export const SELECT_BEGINNER = 'beginner';
export const SELECT_PRO = 'pro';

export const SELECT_TRANSPORT = 'transport';
export const SELECT_HR = 'employer';
export const SELECT_REALTY = 'realestate';
export const SELECT_SALES = 'goods';
export const SELECT_SERVICES = 'services';

export const SELECT_AVITO_PPORTUNITIES = 'avitoOpportunities';
export const SELECT_BUILDSALES = 'buildsales';
export const SELECT_RUNBUSINESS = 'runbusiness';
export const SELECT_ATTRACT_СUSTOMERS = 'attractСustomers';
