import { createContext, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { SELECT_ANY } from 'constants/recommendations';

const defaultExperienceValue = {
  value: SELECT_ANY,
  title: 'Без опыта',
};

const defaultBusinessValue = {
  value: SELECT_ANY,
  title: 'Любая',
};

const defaultLearnValue = {
  value: SELECT_ANY,
  title: 'Всему',
};

export const FilterContext = createContext();

export function FilterProvider({ children }) {
  const [valueExperience, setValueExperience] = useState(
    defaultExperienceValue,
  );

  const [valueBusiness, setValueBusiness] = useState(defaultBusinessValue);

  const [valueLearn, setValueLearn] = useState(defaultLearnValue);

  const value = useMemo(
    () => ({
      valueExperience,
      setValueExperience,
      valueBusiness,
      setValueBusiness,
      valueLearn,
      setValueLearn,
    }),
    [
      valueExperience,
      setValueExperience,
      valueBusiness,
      setValueBusiness,
      valueLearn,
      setValueLearn,
    ],
  );

  return (
    <FilterContext.Provider value={value}>{children}</FilterContext.Provider>
  );
}

FilterProvider.propTypes = {
  children: PropTypes.node,
};

FilterProvider.defaultProps = {
  children: null,
};

FilterProvider.displayName = 'FilterProvider';
