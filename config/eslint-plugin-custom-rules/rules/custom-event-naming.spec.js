const rule = require('../rules/custom-event-naming');
const { RuleTester } = require('eslint');

const ruleTester = new RuleTester({
    parser: require.resolve('@babel/eslint-parser'),
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {},
        requireConfigFile: false
    }
});

ruleTester.run('custom-event-naming', rule, {
    valid: [
        `new CustomEvent('single-page:updateCatalog')`,
        `new CustomEvent('package:action')`,
        `new CustomEvent(eventName)`
    ],
    invalid: [
        {
            code: `new CustomEvent('action')`,
            errors: [{
                message: `CustomEvent name should follow pattern 'module:action', got 'action' instead`,
                type: 'Literal'
            }]
        }
    ]
});
