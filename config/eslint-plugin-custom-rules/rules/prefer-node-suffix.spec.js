const rule = require('./prefer-node-suffix');
const { RuleTester } = require('eslint');

const ruleTester = new RuleTester({
    parser: require.resolve('@babel/eslint-parser'),
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {},
        requireConfigFile: false
    }
});

ruleTester.run('prefer-node-suffix', rule, {
    valid: [
        'const itemsNode = document.querySelector(".js-items");',
        'const obj = {}; obj.tabsNode = itemsNode.querySelector(".js-tabs");'
    ],
    invalid: [
        {
            code: 'const items = document.querySelector(".js-items");',
            errors: [
                {
                    message: 'Use postfix Node, when accessing DOM nodes, items => itemsNode',
                    type: ''
                }
            ]
        },
        {
            code: 'const obj = {}; obj.list = document.querySelector(".js-list");',
            errors: [
                {
                    message: 'Use postfix Node, when accessing DOM nodes, list => listNode',
                    type: ''
                }
            ]
        }
    ]
});
