const rule = require('./prefer-js-prefix');
const { RuleTester } = require('eslint');

const ruleTester = new RuleTester({
    parser: require.resolve('@babel/eslint-parser'),
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {},
        requireConfigFile: false
    }
});

ruleTester.run('prefer-node-suffix', rule, {
    valid: [
        'const itemsNode = document.querySelector(".js-items");',
        'const obj = {}; obj.tabsNode = itemsNode.querySelector(".js-tabs");',
        'const itemsNode = document.querySelectorAll(".js-items");',
        'const obj = {}; obj.tabsNode = itemsNode.querySelectorAll(".js-tabs");',
        'const body = document.body; const someNode = body.querySelectorAll(".js-some-node");'
    ],
    invalid: [
        {
            code: 'const items = document.querySelector(".items");',
            errors: [
                {
                    message: 'Use js-* prefix for accessing DOM nodes',
                    type: ''
                }
            ]
        },
        {
            code: 'const obj = {}; obj.list = document.querySelector(".list");',
            errors: [
                {
                    message: 'Use js-* prefix for accessing DOM nodes',
                    type: ''
                }
            ]
        }
    ]
});
