/**
 * getClassBodyNode
 * @param {Object} context
 * @returns {string|null}
 */
function getClassBodyNode(context) {
    const ancestors = context.getAncestors();

    for (let i = 0; i < ancestors.length; i++) {
        if (ancestors[i].type === 'ClassBody') {
            return ancestors[i];
        }
    }

    return null;
}

/**
 * create
 * @param {Object} context
 * @returns {Object}
 */
function create(context) {
    const bindingsMap = new Map();
    const methodsMap = new Map();
    const sourceCode = context.getSourceCode();
    const regexp = context.options[0] || '/handle.*/';
    const callExpressionParams = ['callee.property.name="bind"', 'arguments.length', 'callee.computed=false', 'callee.object.computed=false', 'callee.object.property.name=' + regexp, 'callee.object.object.type="ThisExpression"'];
    const callExpressionSelector = `ClassBody :not(CallExpression) > CallExpression[${callExpressionParams.join('][')}]`;
    const methodDefinitionParams = ['key.name=' + regexp, 'computed=false'];
    const methodDefinitionSelector = `ClassBody MethodDefinition[${methodDefinitionParams.join('][')}]`;

    return {
        [callExpressionSelector](node) {
            if (node.arguments[0].type !== 'ThisExpression') {
                return;
            }

            const { object } = node.callee;
            const methodName = object.property.name;
            const classBody = getClassBodyNode(context);
            let mapItem = bindingsMap.get(classBody);

            if (!mapItem) {
                mapItem = Object.create(null);
                bindingsMap.set(classBody, mapItem);
            }

            if (!mapItem[methodName]) {
                mapItem[methodName] = [];
                mapItem[methodName].maxArgs = 0;
            }

            mapItem[methodName].maxArgs = Math.max(mapItem[methodName].maxArgs, node.arguments.length);
            mapItem[methodName].push(node);
        },
        [methodDefinitionSelector](node) {
            const classBody = getClassBodyNode(context);
            const methodName = node.key.name;
            let mapItem = methodsMap.get(classBody);

            if (!mapItem) {
                mapItem = Object.create(null);
                methodsMap.set(classBody, mapItem);
            }

            mapItem[methodName] = node;
        },
        'Program:exit'() {
            methodsMap.forEach((methods, classNode) => {
                for (const methodName in methods) { // eslint-disable-line guard-for-in
                    const bindData = bindingsMap.get(classNode);

                    if (bindData && bindData[methodName] && bindData[methodName].maxArgs === 1) {
                        const node = methods[methodName];

                        context.report({
                            node: node,
                            message: 'Define this method as an arrow function class property',
                            fix(fixer) {
                                const newText = sourceCode.getText(node).replace(/^(.+?)(\([^)]*?\))(([\s\S]+))/, '$1 = $2 =>$3;');

                                return fixer.replaceText(node, newText);
                            }
                        });
                    }
                }
            });

            bindingsMap.forEach((bindings) => {
                for (const methodName in bindings) {
                    if (bindings[methodName].maxArgs !== 1) {
                        continue;
                    }

                    const bindingNodes = bindings[methodName];

                    for (let i = 0; i < bindingNodes.length; i++) {
                        const node = bindingNodes[i];

                        context.report({
                            node: node,
                            message: `Define "${methodName}" as an arrow function class property instead of binding a context`,
                            fix(fixer) {
                                // todo: удалять конструктор, если там больше ничего нет, кроме super(props)
                                return fixer.remove(node.parent);
                            }
                        });
                    }
                }
            });

            methodsMap.clear();
            bindingsMap.clear();
        }
    };
}

module.exports = {
    meta: { fixable: 'code' },
    create
};
