'use strict';

const { getNestedProperty } = require('../utils');

module.exports = {
    meta: {
        docs: {
            description: `CustomEvent name should follow pattern 'module:action'`,
            category: 'Stylistic Issues',
            recommended: false
        }
    },

    create: function(context) {
        return {
            NewExpression: function(node) {
                const constructorName = getNestedProperty(node, 'callee.name');
                if (constructorName !== 'CustomEvent') {
                    return;
                }
                const [arg] = node.arguments;
                const eventNameRegex = /^.+:.+$/;
                if (arg && ['Literal', 'StringLiteral'].includes(arg.type) && !eventNameRegex.test(arg.value)) {
                    context.report({
                        node: arg,
                        message: `CustomEvent name should follow pattern 'module:action', got '${arg.value}' instead`
                    });
                }
            }
        };
    }
};
