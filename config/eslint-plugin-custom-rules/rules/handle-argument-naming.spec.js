const rule = require('../rules/handle-argument-naming');
const { RuleTester } = require('eslint');

const ruleTester = new RuleTester({
    parser: require.resolve('@babel/eslint-parser'),
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {},
        requireConfigFile: false,
        babelOptions: {
            plugins: [
                '@babel/plugin-proposal-class-properties'
            ]
        }
    }
});

ruleTester.run('handle-argument-naming', rule, {
    valid: [
        `class Test extends PureComponent {
            handleClick = (event) => {}
        }`,
        `class Test extends PureComponent {
            handleClick = event => {}
        }`,
        `class Test extends PureComponent {
            handleClick = () => {}
        }`,
        `class Test extends PureComponent {
            handleClick = ({name, value}) => {}
        }`,
        `class Test extends PureComponent {
            handleClick = (event, otherArgument) => {}
        }`
    ],
    invalid: [
        {
            code: `class Test extends PureComponent {
                        handleClick = (e) => {}
                    }`,
            errors: [{
                message: 'First argument of handleClick method must be called \'event\', got \'e\' instead',
                type: 'Identifier'
            }]
        }
    ]
});
