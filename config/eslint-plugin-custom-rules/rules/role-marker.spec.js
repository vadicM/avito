const rule = require('./role-marker');
const { RuleTester } = require('eslint');

const ruleTester = new RuleTester({
    parser: require.resolve('@babel/eslint-parser'),
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        },
        requireConfigFile: false,
        babelOptions: {
            presets: [
                '@babel/preset-react'
            ],
            plugins: [
                '@babel/plugin-proposal-class-properties'
            ]
        }
    }
});

const options = [{ prop: 'marker', attr: 'data-marker', spreadName: ['props', 'role'] }];

ruleTester.run('role-marker', rule, {
    valid: [
        // user components
        {
            code: `<MyComp onClick={() => {}} marker='marker' />`,
            options
        },
        {
            code: `<MyComp key='1' marker='marker' markerId='1' />`,
            options
        },
        {
            code: `<MyComp key='1' marker='marker' markerId='1' />`,
            options
        },
        {
            code: `<MyComp key='1' {...props} />`,
            options
        },
        {
            code: `<MyComp key='1' {...this.props} />`,
            options
        },
        {
            code: `
            class MyComp {
                render() {
                    return <MyComp marker='marker' />
                }
            }
            `,
            options
        },
        {
            code: `
            class MyComp {
                render() {
                    return (
                        <MyComp marker='marker'>
                            <div>...</div>
                            {list.map(name => <MyItem name={name} />)}
                        </MyComp>
                    );
                }
            }
            `,
            options
        },
        // ========================
        // native components
        {
            code: `<div onClick={() => {}} data-marker='marker' />`,
            options
        },
        {
            code: `<div key='1' data-marker='marker(1)' />`,
            options
        },
        {
            code: `<div key='1' {...role({})} />`,
            options
        },
        {
            code: `<div key='1' {...props} />`,
            options
        },
        {
            code: `<div key='1' {...this.props} />`,
            options
        },
        {
            code: `
            class MyComp {
                render() {
                    return <div data-marker='marker' />
                }
            }
            `,
            options
        },
        {
            code: `
            class MyComp {
                render() {
                    return (
                        <div data-marker='marker'>
                            <div>...</div>
                            {list.map(name => <MyItem name={name} />)}
                        </div>
                    );
                }
            }
            `,
            options
        }
    ],

    invalid: [
        // user components
        {
            code: `<MyComp onClick={() => {}} />`,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a component with an event handler'
                }
            ]
        },
        {
            code: `<MyComp key='1' />`,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a component with a key'
                },
                {
                    message: 'You should add markerId property with some value (role ID) for a component with a key'
                }
            ]
        },
        {
            code: `<MyComp key='1' marker='marker' />`,
            options,
            errors: [
                {
                    message: 'You should add markerId property with some value (role ID) for a component with a key'
                }
            ]
        },
        {
            code: `<MyComp key='1' markerId='markerId' />`,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a component with a key'
                }
            ]
        },
        {
            code: `<MyComp key='1' {...r({})} />`,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a component with a key'
                },
                {
                    message: 'You should add markerId property with some value (role ID) for a component with a key'
                }
            ]
        },
        {
            code: `<MyComp key='1' {...p} />`,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a component with a key'
                },
                {
                    message: 'You should add markerId property with some value (role ID) for a component with a key'
                }
            ]
        },
        {
            code: `<MyComp key='1' {...this.p} />`,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a component with a key'
                },
                {
                    message: 'You should add markerId property with some value (role ID) for a component with a key'
                }
            ]
        },
        {
            code: `
            class MyComp {
                render() {
                    return (
                        <MyComp>
                            <div>...</div>
                            {list.map(name => <MyItem name={name} />)}
                        </MyComp>
                    );
                }
            }
            `,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a render root',
                    line: 5
                }
            ]
        },
        {
            code: `
            class MyComp {
                render() {
                    if (some) {
                        return (
                            <MyComp>
                                <div>...</div>
                                {list.map(name => <MyItem name={name} />)}
                            </MyComp>
                        );
                    } else {
                        return (
                            <MyOtherComp>
                                <div>...</div>
                                {list.map(name => <MyItem name={name} />)}
                            </MyOtherComp>
                        );
                    }
                }
            }
            `,
            options,
            errors: [
                {
                    message: 'You should add marker property with some value (role) for a render root',
                    line: 6
                },
                {
                    message: 'You should add marker property with some value (role) for a render root',
                    line: 13
                }
            ]
        },
        // ========================
        // native components
        {
            code: `<div onClick={() => {}} />`,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) for a component with an event handler'
                }
            ]
        },
        {
            code: `<div onClick={() => {}} marker='marker' />`,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) for a component with an event handler'
                }
            ]
        },
        {
            code: `<div key='1' />`,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) and role ID for a component with a key'
                }
            ]
        },
        {
            code: `<div key='1' marker='marker' markerId='1'  />`,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) and role ID for a component with a key'
                }
            ]
        },
        {
            code: `<div key='1' {...r({})} />`,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) and role ID for a component with a key'
                }
            ]
        },
        {
            code: `<div key='1' {...p} />`,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) and role ID for a component with a key'
                }
            ]
        },
        {
            code: `<div key='1' {...this.p} />`,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) and role ID for a component with a key'
                }
            ]
        },
        {
            code: `
            class MyComp {
                render() {
                    return (
                        <div>
                            <div>...</div>
                            {list.map(name => <MyItem name={name} />)}
                        </div>
                    );
                }
            }
            `,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) for a render root',
                    line: 5
                }
            ]
        },
        {
            code: `
            class MyComp {
                render() {
                    if (some) {
                        return (
                            <div>
                                <div>...</div>
                                {list.map(name => <MyItem name={name} />)}
                            </div>
                        );
                    } else {
                        return (
                            <div>
                                <div>...</div>
                                {list.map(name => <MyItem name={name} />)}
                            </div>
                        );
                    }
                }
            }
            `,
            options,
            errors: [
                {
                    message: 'You should add data-marker attribute with some value (role) for a render root',
                    line: 6
                },
                {
                    message: 'You should add data-marker attribute with some value (role) for a render root',
                    line: 13
                }
            ]
        }
    ]
});
