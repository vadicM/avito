const NODE_SUFFIX = 'Node';

/**
 * Генератор отчета
 * @param {string} name ошибочное имя
 * @param {string} correctSuffix верный суффикс
 * @return {string}
 */
function reportGenerator(name, correctSuffix = NODE_SUFFIX) {
    return `Use postfix Node, when accessing DOM nodes, ${name} => ${name.replace(/Nod.+$/, '')}${correctSuffix}`;
}

/**
 * Является ли querySelector
 * @param {string} name
 * @return {boolean}
 */
function isQuerySeletor(name) {
    return name === 'querySelector';
}

module.exports = {
    create: function(context) {
        return {
            AssignmentExpression: function(node) {
                if (
                    node &&
                    node.right &&
                    node.right.callee &&
                    node.right.callee.property &&
                    node.left &&
                    node.left.property &&
                    isQuerySeletor(node.right.callee.property.name) &&
                    !node.left.property.name.endsWith(NODE_SUFFIX)
                ) {
                    context.report(
                        node,
                        reportGenerator(node.left.property.name)
                    );
                }
            },
            VariableDeclarator: function(node) {
                if (
                    node &&
                    node.id &&
                    node.id.name &&
                    node.init &&
                    node.init.callee &&
                    node.init.callee.property &&
                    isQuerySeletor(node.init.callee.property.name) &&
                    !node.id.name.endsWith(NODE_SUFFIX)
                ) {
                    context.report(node, reportGenerator(node.id.name));
                }
            }
        };
    }
};
