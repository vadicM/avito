'use strict';

const { getNestedProperty } = require('../utils');

module.exports = {
    meta: {
        docs: {
            description: 'handle* method`s first argument must be called `event`',
            category: 'Stylistic Issues',
            recommended: false
        },
        fixable: 'code'
    },

    create: function(context) {
        const requiredVariableName = 'event';
        return {
            ClassProperty: function(node) {
                const methodName = getNestedProperty(node, 'key.name');
                if (!/^handle[A-Z]\w+$/.test(methodName)) {
                    return;
                }
                const [arg] = getNestedProperty(node, 'value.params');
                if (arg && arg.type === 'Identifier' && arg.name !== requiredVariableName) {
                    context.report({
                        node: arg,
                        message: `First argument of ${methodName} method must be called '${requiredVariableName}', got '${arg.name}' instead`
                    });
                }
            }
        };
    }
};
