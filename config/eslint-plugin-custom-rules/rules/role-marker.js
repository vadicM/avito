/**
 * isRenderRoot
 * @param {Object} context
 * @returns {boolean}
 */
function isRenderRoot(context) {
    const ancestors = context.getAncestors();

    for (let i = ancestors.length; --i;) {
        const node = ancestors[i];

        // нас интересуют только корневые элементы, находящиеся внутри метода с именем render
        // если элемент находится внутри функции, то нет смысла дальше перебирать владельцев
        if (node.type === 'MethodDefinition') {
            return node.key.name === 'render';
        } else if (node.type === 'FunctionExpression' && ancestors[i - 1].type === 'MethodDefinition') {
            return ancestors[i - 1].key.name === 'render';
        } else if (node.type === 'JSXElement' || node.type === 'ArrowFunctionExpression') {
            return false;
        }
    }

    return false;
}

/**
 * validateNode
 * @param {Object} context
 * @param {Object} node
 * @param {Function} isRenderRoot
 * @returns {undefined}
 */
function validateNode(context, node, isRenderRoot) {
    const { prop: markerProp = 'marker', attr: markerAttr = 'data-marker', spreadName = ['props', 'role'] } = context.options[0] || {};
    const opening = node.openingElement;
    const { attributes } = opening;
    const attrMap = {};
    let isUserComponent;

    if (opening.name.type === 'JSXMemberExpression') {
        isUserComponent = true;
    } else {
        const componentName = opening.name.name;
        isUserComponent = componentName[0] >= 'A' && componentName[0] <= 'Z';
    }

    for (let i = 0; i < attributes.length; i++) {
        const attribute = attributes[i];

        if (attribute.type === 'JSXAttribute') {
            const { name } = attributes[i].name;

            if (name === 'key') {
                attrMap.key = true;
            } else if (name.indexOf('on') === 0) {
                attrMap.handler = true;
            } else if (name === markerProp) {
                attrMap.marker = true;
            } else if (name === markerProp + 'Id') {
                attrMap.markerId = true;
            } else if (name === markerAttr) {
                attrMap.markerAttr = true;
            }
        } else if (attribute.type === 'JSXSpreadAttribute') {
            if (attribute.argument.type === 'CallExpression' && spreadName.includes(attribute.argument.callee.name)) {
                attrMap.spreadFn = true;
            } else if (attribute.argument.type === 'MemberExpression' && spreadName.includes(attribute.argument.property.name)) {
                attrMap.spreadFn = true;
            } else if (attribute.argument.type === 'ObjectExpression') {
                attrMap.spreadFn = true;
            } else if (attribute.argument.type === 'Identifier' && spreadName.includes(attribute.argument.name)) {
                attrMap.spreadFn = true;
            }
        }
    }

    if (attrMap.spreadFn) {
        return;
    }

    if (isRenderRoot) {
        if (isUserComponent) {
            if (!attrMap.marker) {
                context.report({
                    node,
                    message: `You should add ${markerProp} property with some value (role) for a render root`
                });
            }
        } else if (!attrMap.markerAttr) {
            context.report({
                node,
                message: `You should add ${markerAttr} attribute with some value (role) for a render root`
            });
        }
    }

    if (attrMap.key) {
        if (isUserComponent) {
            if (!attrMap.marker) {
                context.report({
                    node,
                    message: `You should add ${markerProp} property with some value (role) for a component with a key`
                });
            }
            if (!attrMap.markerId) {
                context.report({
                    node,
                    message: `You should add ${markerProp}Id property with some value (role ID) for a component with a key`
                });
            }
        } else if (!attrMap.markerAttr) {
            context.report({
                node,
                message: `You should add ${markerAttr} attribute with some value (role) and role ID for a component with a key`
            });
        }
    }

    if (attrMap.handler) {
        if (isUserComponent) {
            if (!attrMap.marker) {
                context.report({
                    node,
                    message: `You should add ${markerProp} property with some value (role) for a component with an event handler`
                });
            }
        } else if (!attrMap.markerAttr) {
            context.report({
                node,
                message: `You should add ${markerAttr} attribute with some value (role) for a component with an event handler`
            });
        }
    }
}

/**
 * create
 * @param {Object} context
 * @returns {Object}
 */
function create(context) {
    return {
        JSXElement(node) {
            validateNode(context, node, isRenderRoot(context));
        }
    };
}

module.exports = {
    create
};
