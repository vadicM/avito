/**
 * Получить вложенное свойство
 * @param {Object} object
 * @param {string} path
 * @return {?}
 */
function getNestedProperty(object = {}, path = '') {
    return path.split('.').reduce((prop, step) => {
        if (prop && prop[step]) {
            return prop[step];
        }

        return '';
    }, object);
}

module.exports = { getNestedProperty };
