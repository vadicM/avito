module.exports.rules = {
    'no-bind-on-handler': require('./rules/no-bind-on-handler'),
    'prefer-node-suffix': require('./rules/prefer-node-suffix'),
    'prefer-js-prefix': require('./rules/prefer-js-prefix'),
    'role-marker': require('./rules/role-marker'),
    'custom-event-naming': require('./rules/custom-event-naming'),
    'handle-argument-naming': require('./rules/handle-argument-naming')
};
