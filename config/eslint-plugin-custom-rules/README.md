# eslint-plugin-custom-rules

## Что это?

Это набор кастомных правил для `eslint`.

## Как использовать

`npm install @avito/eslint-plugin-custom-rules --save-dev`

> .eslintrc:

```json
{
  "plugins": [
    "@avito/custom-rules"
  ],
  "rules": {
    "@avito/custom-rules/[rule-name]": 2
  }
}
```

## Запуск тестов правил

Тесты расположены в `./tests` с названиями `${plugin-name}.spec.js`, для запуска необходимо выполнить команду:

```bash
npm install // Если еще не были установлены зависимости
npm test // npm t

```

---

## no-bind-on-handler

Правило, которое помогает не забывать использовать arrow function в class props вместо привязки контекст.

```jsx harmony
class Component extends PureComponent {
  constructor(props) {
    super(props);
    
    this.handleDragOver = this.handleDragOver.bind(this);
    this.handleDrop = this.handleDrop.bind(this);
    this.handleDragEnd = this.handleDragEnd.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  
  render() {
    return (
      <div
        onDragOver={this.handleDragOver}
        onDrop={this.handleDrop}
        onDragEnd={this.handleDragEnd}
        onClick={this.handleClick}>
        drop here
      </div>
    );
  }

  handleDragOver(event) {
    // ...
  }

  handleDrop(event) {
    // ...
  }

  handleDragEnd(event) {
    // ...
  }

  handleClick(event) {
    // ...
  }
}
```

Превращается в

```jsx harmony
class Component extends PureComponent {
  render() {
    return (
      <div
        onDragOver={this.handleDragOver}
        onDrop={this.handleDrop}
        onDragEnd={this.handleDragEnd}
        onClick={this.handleClick}>
        drop here
      </div>
    );
  }

  handleDragOver = (event) => {
    // ...
  };

  handleDrop = (event) => {
    // ...
  };

  handleDragEnd = (event) => {
    // ...
  };

  handleClick = (event) => {
    // ...
  }
}
```

> Правило поддерживает автоправку через `--fix`

### Зачем

Уменьшается количество кода (не нужно использовать bind, часто не нужно перегружать конструктор)

### Как использовать?

> .eslintrc:

```json
{
  "plugins": [
    "@avito/custom-rules"
  ],
  "rules": {
    "@avito/custom-rules/no-bind-on-handler": [1, "/handle.*/"]
  }
}
```

У правила есть один аргумент - регулярное выражение, по которому будут определяться обработчики.

> По умолчанию `/handle.*/`

---

## prefer-node-suffix

Правило форсирующее применения [правила](https://cf.avito.ru/pages/viewpage.action?pageId=19990700#id-Процессы-JS) наименования DOM нод в переменных.

```javascript
// не ок
var element = document.querySelector('.js-element');
  
// ок
var elementNode = document.querySelector('.js-element');
```

### Зачем

Сохранение код стайла, сокращение времении на PR новых разработчиков, которые по какой-либо причине могли упустить это правило.

### Как использовать?

> .eslintrc:

```json
{
  "plugins": [
    "@avito/custom-rules"
  ],
  "rules": {
    "@avito/custom-rules/prefer-node-suffix": 1
  }
}
```

---

## role-marker

Правило заставляющее ставить [роль маркеры](https://cf.avito.ru/pages/viewpage.action?pageId=45578218) на элементах и передавать role-props компонентам.

### Зачем

Разметка кода роль-маркерами для упрощения автоматизированного тестирования.

### Как использовать?

> .eslintrc:

```json
{
  "plugins": [
    "@avito/custom-rules"
  ],
  "rules": {
    "@avito/custom-rules/role-marker": [1, { "prop": "marker", "attr": "data-marker", "spreadFn": "role" }]
  }
}
```

Правило заставляет расставлять роль-маркеры и role-props на следующих элементах/компонентах:

#### Render root

Корневой элемент компонента:

Плохо:

```jsx
class MyComp {
    render() {
        return (
            <MyComp>
                <div>...</div>
            </div>
        );
    } 
}
```

```jsx
class MyComp {
    render() {
        return (
            <div>
                <div>...</div>
            </div>
        );
    } 
}
```

Хорошо:

```jsx
class MyComp {
    render() {
        return (
            <MyComp marker='some-marker'>
                <div>...</div>
            </div>
        );
    } 
}
```

```jsx
class MyComp {
    render() {
        return (
            <div data-marker='some-marker'>
                <div>...</div>
            </div>
        );
    } 
}
```

#### Компоненты с обработчиками

Плохо:

```jsx
<MyComp onClick={() => {}} />
<button onClick={() => {}} />
```

Хорошо:

```jsx
<MyComp onClick={() => {}} marker='some-marker' />
<button onClick={() => {}} data-marker='some-marker' />
```

#### Элементы списков (с key)

Плохо:

```jsx
<MyComp>
    {list.map(({ id, name }) => <MyItem key={id} name={name} />)}
</MyComp>
<MyComp>
    {list.map(({ id, name }) => <div key={id}>{name}</div>)}
</MyComp>
```

Хорошо:

```jsx
<MyComp>
    {list.map(({ id, name }) => <MyItem key={id} marker='some-marker' markerId={id} name={name} />)}
</MyComp>
<MyComp>
    {list.map(({ id, name }) => <div key={id} data-marker={`some-marker(${id})`}>{name}</div>)}
</MyComp>
```

#### Spread

Линтер проигнорирует элемент/компонент в том случае, если среди его атрибутов/свойств есть spread с аргументами:

- `props`: `<MyComp {...props} />`, `<div {...props}>...</div>`
- `this.props`: `<MyComp {...this.props} />`, `<div {...this.props}>...</div>`
- `rule`: `<MyComp {...rule(this.props)} />`, `<div {...rule(this.props)}>...</div>`
